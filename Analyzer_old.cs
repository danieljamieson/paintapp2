﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Media;
using System.Windows;

namespace PaintApp1
{
    public class Analyzer
    {
        //private Logger logger;
        public Analyzer()
        {
        }

        public int guessOptimiumDelayFix(TouchGesture tg, IMUGesture imug) {
            if (tg.gestureData.Count <= 2)
            {
                return 0;
            }
            double corD;
            double[] xs;
            double[] ys;
            double[] xvels;
            double[] yvels;
            double[] xaccs;
            double[] yaccs;
            double[] daccs = new double[tg.gestureData.Count]; ;
            double[] actualTimes;
            double[] resampledTimes;
            double[] imuLinXVals = new double[tg.gestureData.Count];
            double[] imuLinYVals = new double[tg.gestureData.Count];
            double[] imuLinDVals = new double[tg.gestureData.Count];
            double[] actualIMUTimes = new double[tg.gestureData.Count];

            xs = new double[tg.gestureData.Count()];
            ys = new double[tg.gestureData.Count()];
            actualTimes = new double[xs.Length];
            int i, j;
            TouchGesPoint tp;

            int bestDelay = 0;
            double bestCorD = -2;

            for (int delay = -5; delay <= 5; delay++)
            {
                if ((tg.gestureData.Count + imug.indexOfGestureInList + delay) > imug.gestureData.Count)
                    continue;
                for (i = 0, j = imug.indexOfGestureInList + delay; i < tg.gestureData.Count; i++, j++)
                {
                    tp = tg.gestureData[i];
                    xs[i] = tp.x;
                    ys[i] = tp.y;
                    actualTimes[i] = tp.actualTime;

                    actualIMUTimes[i] = imug.gestureData[j].actualTime;
                    imuLinXVals[i] = imug.gestureData[j].linearAcc[0];
                    imuLinYVals[i] = imug.gestureData[j].linearAcc[1];
                }
                double samplingInterval;
                double[] dummy = new double[xs.Length];
                xs = resample(xs, actualTimes, out resampledTimes, out samplingInterval);
                ys = resample(ys, actualTimes, out resampledTimes, out samplingInterval);
                xs = filterData3(xs);
                ys = filterData3(ys);

                imuLinXVals = resample(imuLinXVals, actualIMUTimes, samplingInterval);
                imuLinYVals = resample(imuLinYVals, actualIMUTimes, samplingInterval);

                double vxRange, vyRange;
                xvels = getDerivatives(xs, resampledTimes, out vxRange);
                yvels = getDerivatives(ys, resampledTimes, out vyRange);

                double axRange, ayRange;
                xaccs = getDerivatives(xvels, resampledTimes, out axRange);
                yaccs = getDerivatives(yvels, resampledTimes, out ayRange);

                xaccs[1] = 0;   //it is incorrect because v[0] is unknown
                yaccs[1] = 0;   //it is incorrect because v[0] is unknown

                imuLinXVals[0] = imuLinXVals[1] = 0; //to give it the same initial conditions as the touch data
                imuLinYVals[0] = imuLinYVals[1] = 0; //to give it the same initial conditions as the touch data

                imuLinXVals[xaccs.Length - 1] = 0;
                imuLinYVals[xaccs.Length - 1] = 0;

                //to level the playing field
                xaccs[xaccs.Length - 1] = 0;
                yaccs[xaccs.Length - 1] = 0;

                int filterVal = 5;
                xaccs = filterData(xaccs, filterVal);
                yaccs = filterData(yaccs, filterVal);
                imuLinXVals = filterData(imuLinXVals, filterVal);
                imuLinYVals = filterData(imuLinYVals, filterVal);

                for (i = 0; i < xaccs.Length; i++)
                {
                    daccs[i] = Math.Sqrt(Math.Pow(xaccs[i], 2) + Math.Pow(yaccs[i], 2));
                    imuLinDVals[i] = Math.Sqrt(Math.Pow(imuLinXVals[i], 2) + Math.Pow(imuLinYVals[i], 2));
                }

                corD = getCorrelation(daccs, imuLinDVals);
                if (corD > bestCorD)
                {
                    bestCorD = corD;
                    bestDelay = delay;
                }
            }
            return bestDelay;
        }


        /// <summary>
        /// Calculates the correlation between touchGesture and IMUGesture data for distance, x, y, and angle using the transofmationAngle and Delay provided.
        /// </summary>
        /// <param name="tg">The TouchGesture: from a touch screen, or even mouse data</param>
        /// <param name="imug">the IMUGesture: from the imu data</param>
        /// <param name="corX">Output parameter to return the correlation with respect to acceleration along the X asis</param>
        /// <param name="corY">Output parameter to return the correlation with respect to acceleration along the Y asis</param>
        /// <param name="corD">Output parameter to return the correlation with respect to acceleration distance</param>
        /// <param name="corAngle">Output parameter to return the correlation with respect to change an Angle</param>
        /// <param name="rotTrans">Rotation transform to be applied to the tg data to match its orientation to imug</param>
        /// <param name="delay">a time correction to be applied to the imug data. Basically, the imug includes 30 readings before and 30 readings after the 
        /// duration of the touchEvent at 50msec intervals. For a matching signal, we should ignore the first and last 30 reading. However, due to timing
        /// issues, sometimes we get better results if we go back or forward 1 or 2 samples. delay is basically the number of samples to shift and in 
        /// which direction</param>
        /// <param name="filterVal">The value of the moving average filter to use. It can be 0 (no filter), 3, or 5</param>
        /// <param name="bDraw">A UI flag to indicate whether to draw graphs while duing the calculations or not</param>
        /// <returns>void</returns>
        public void getCorrelationFromRawGestureData(TouchGesture tg, IMUGesture imug, out double corX, out double corY, out double corD, out double corAngle, RotateTransform rotTrans, int delay, int filterVal, bool bDraw)
        {
            if (tg.gestureData.Count <= 2)
            {
                corX = corY = corD = corAngle = 0;
                return;
            }
            double[] xs;
            double[] ys;
            double[] xvels;
            double[] yvels;
            double[] xaccs;
            double[] yaccs;
            double[] daccs = new double[tg.gestureData.Count]; ;
            double[] deltaAngles;
            double[] actualTimes;
            double[] resampledTimes;
            double[] imuLinXVals = new double[tg.gestureData.Count];
            double[] imuLinYVals = new double[tg.gestureData.Count];
            double[] imuLinDVals = new double[tg.gestureData.Count];
            double[] imuDeltaAngles;
            double[] actualIMUTimes = new double[tg.gestureData.Count];
            
            xs = new double[tg.gestureData.Count()];
            ys = new double[tg.gestureData.Count()];
            actualTimes = new double[xs.Length];
            double xMax=double.MinValue, yMax=double.MinValue;
            double xMin = double.MaxValue, yMin = double.MaxValue;
            int i,j;
            TouchGesPoint tp;

            double imuXMin = double.MaxValue, imuXMax = double.MinValue;
            double imuYMin = double.MaxValue, imuYMax = double.MinValue;
            
            //get x,y,time and transform coordinates if needed
            //if ((tg.gestureData.Count + 30 + delay) > imug.gestureData.Count)
            //{
            //    corX = corY = corD = corAngle = 0;
            //    return;
            //}
            //setting j to 32 gave best results
            for (i = 0, j = imug.indexOfGestureInList + delay; i < tg.gestureData.Count; i++, j++) 
            {
                tp = tg.gestureData[i];
                Point tranPt = new Point(tp.x, tp.y);
                tranPt = rotTrans.Transform(tranPt);
                xs[i] = tranPt.X;
                ys[i] = tranPt.Y;
                actualTimes[i] = tp.actualTime;
                if (xs[i] > xMax)
                    xMax = xs[i];
                else if (xs[i] < xMin)
                    xMin = xs[i];
                if (ys[i] > yMax)
                    yMax = ys[i];
                else if (ys[i] < yMin)
                    yMin = ys[i];

                if (j < imug.gestureData.Count)
                {
                    actualIMUTimes[i] = imug.gestureData[j].actualTime;
                    imuLinXVals[i] = imug.gestureData[j].linearAcc[0];
                    imuLinYVals[i] = imug.gestureData[j].linearAcc[1];
                    if (imuLinXVals[i] > imuXMax)
                        imuXMax = imuLinXVals[i];
                    else if (imuLinXVals[i] < imuXMin)
                        imuXMin = imuLinXVals[i];
                    if (imuLinYVals[i] > imuYMax)
                        imuYMax = imuLinYVals[i];
                    else if (imuLinYVals[i] < imuYMin)
                        imuYMin = imuLinYVals[i];
                }
            }

            double samplingInterval;
            double[] dummy = new double[xs.Length];
            //xs = resample("xs", logger, xs, actualTimes, out resampledTimes, out samplingInterval);
            //ys = resample("ys", logger, ys, actualTimes, out resampledTimes, out samplingInterval);
            xs = resample(xs, actualTimes, out resampledTimes, out samplingInterval);
            ys = resample(ys, actualTimes, out resampledTimes, out samplingInterval);

            xs = filterData(xs, filterVal);
            ys = filterData(ys, filterVal);
            //deltaAngles = getDeltaAngles(xs, ys, xMax - xMin, yMax - yMin);

            //imuLinXVals = resample("imuX", logger, imuLinXVals, actualIMUTimes, samplingInterval);
            //imuLinYVals = resample("imuY", logger, imuLinYVals, actualIMUTimes, samplingInterval);
            imuLinXVals = resample(imuLinXVals, actualIMUTimes, samplingInterval);
            imuLinYVals = resample(imuLinYVals, actualIMUTimes, samplingInterval);

            double vxRange,vyRange;
            xvels = getDerivatives(xs, resampledTimes, out vxRange);
            yvels = getDerivatives(ys, resampledTimes, out vyRange);


            double axRange, ayRange;
            xaccs = getDerivatives(xvels, resampledTimes, out axRange);
            yaccs = getDerivatives(yvels, resampledTimes, out ayRange);

            xaccs[1] = 0;   //it is incorrect because v[0] is unknown
            yaccs[1] = 0;   //it is incorrect because v[0] is unknown

            imuLinXVals[0] = imuLinXVals[1] = 0; //to give it the same initial conditions as the touch data
            imuLinYVals[0] = imuLinYVals[1] = 0; //to give it the same initial conditions as the touch data

            imuLinXVals[xaccs.Length - 1] = 0;
            imuLinYVals[xaccs.Length - 1] = 0;


            //to level the playing field
            xaccs[xaccs.Length-1] = 0;   
            yaccs[xaccs.Length - 1] = 0;


            double[] filteredImuLinXVals = filterData(imuLinXVals, 5);
            double[] filteredImuLinYVals = filterData(imuLinYVals, 5);
            filteredImuLinXVals = filterData(filteredImuLinXVals, 5);
            filteredImuLinYVals = filterData(filteredImuLinYVals, 5);

            if (filterVal != 0)
            {
                xaccs = filterData(xaccs, filterVal);
                yaccs = filterData(yaccs, filterVal);
                imuLinXVals = filterData(imuLinXVals, filterVal);
                imuLinYVals = filterData(imuLinYVals, filterVal);
            }

            for (i = 0; i < xaccs.Length; i++)
            {
                daccs[i] = Math.Sqrt(Math.Pow(xaccs[i], 2) + Math.Pow(yaccs[i], 2));
                imuLinDVals[i] = Math.Sqrt(Math.Pow(imuLinXVals[i], 2) + Math.Pow(imuLinYVals[i], 2));
            }

            //deltaAngles = getDeltaAngles(xaccs, yaccs, xMax - xMin, yMax - yMin);

            imuDeltaAngles = getDeltaAngles(filteredImuLinXVals, filteredImuLinYVals, imuXMax - imuXMin, imuYMax - imuYMin);
            deltaAngles = getDeltaAngles(xs, ys, xMax - xMin, yMax - yMin);
            deltaAngles = filterData(deltaAngles, 5);
            imuDeltaAngles = filterData(imuDeltaAngles, 5);

            if (bDraw)
            {
                //parentWidnow.drawTTGraphs(xaccs, yaccs, daccs);
                //parentWidnow.drawIMUGraphs(imuLinXVals, imuLinYVals, imuLinDVals, resampledTimes, tg.gestureData[0].x, tg.gestureData[0].y);
                //parentWidnow.drawTTGraphs(daccs, deltaAngles, null);
                //parentWidnow.drawIMUGraphs(imuLinDVals, imuDeltaAngles,null, resampledTimes, tg.gestureData[0].x, tg.gestureData[0].y);
            }
            corX = getCorrelation(xaccs, imuLinXVals);
            corY = getCorrelation(yaccs, imuLinYVals);
            corD = getCorrelation(daccs, imuLinDVals);
            corAngle = getCorrelation(deltaAngles, imuDeltaAngles);
        }

        double[] getDerivatives(double[] ds, double[] times, out double valueRange)
        {
            double min=double.MaxValue,max = double.MinValue;
            double[] vels = new double[ds.Length];
            for (int i = 0; i < ds.Length; i++)
            {
                if (i > 0)
                    vels[i] = (ds[i] - ds[i - 1]) / (times[i] - times[i - 1]);
                else
                    vels[i] = 0;
                if (vels[i] > max)
                    max = vels[i];
                if (vels[i] < min)
                    min = vels[i];
            }
            valueRange = max - min;
            return vels;
        }

        //calculates a suitable interval and filles in the resampeTimes
        public static double[] resample(double[] vals, double[] actualTimes, out double[] resampledTimes, out double interval)
        {
            double[] resampledVals = new double[vals.Length];
            resampledTimes = new double[vals.Length];
            double initialTime = actualTimes[0];
            interval = (actualTimes[vals.Length - 1] - initialTime) / (vals.Length - 1);
            double currentTime = initialTime;
            for (int i = 0; i < vals.Length; i++)
            {
                resampledTimes[i] = currentTime;
                if (i > 0 && i < (vals.Length - 1))
                    resampledVals[i] = getInterpolatedValue(i, vals, actualTimes, currentTime);
                else
                    resampledVals[i] = vals[i];
                currentTime += interval;
            }
            return resampledVals;
        }

        //takes in the resample times as input
        public static double[] resample(double[] vals, double[] actualTimes, double samplingInterval)
        {
            double[] resampledVals = new double[vals.Length];
            double currentTime = actualTimes[0];
            for (int i = 0; i < vals.Length; i++)
            {
                if (i > 0 && i < (vals.Length - 1))
                    resampledVals[i] = getInterpolatedValue(i, vals, actualTimes, currentTime);
                else
                    resampledVals[i] = vals[i];
                currentTime += samplingInterval;
            }
            return resampledVals;
        }

        public static double getInterpolatedValue(int i, double[] vals, double[] times, double timeStamp)
        {
            double iVal;
            double slope = (vals[i] - vals[i-1]) / (times[i] - times[i-1]);
            if( (i==1 && timeStamp <= times[i])  ||
                (i==(vals.Length-2) && timeStamp>times[i])){
                iVal = vals[i-1] + (timeStamp - times[i-1]) * slope;
                return iVal;
            }
            double a0, a1, a2, a3, mu2;
            double mu;
            int j;
            if (timeStamp <= times[i])
                j = i;
            else
                j = i + 1;
            mu = (timeStamp - times[j-1]) / (times[j] - times[j-1]);
            mu2 = mu * mu;
            //a0 = y3 - y2 - y0 + y1;
            a0 = vals[j + 1] - vals[j] - vals[j - 2] + vals[j-1];
            //a1 = y0 - y1 - a0;
            a1 = vals[j - 2] - vals[j-1] - a0;
            //a2 = y2 - y0;
            a2 = vals[j] - vals[j - 2];
            //a3 = y1;
            a3 = vals[j-1];

            iVal = (a0 * mu * mu2 + a1 * mu2 + a2 * mu + a3);

            return iVal;
        }

        public static double[] filterData(double[] data, int nFilterPoints)
        {
            //This needs to be replaced with a proper general nPoints moving average filter
            //that takes into account the special characteristics of the touch surface signal
            //and the possibility of a small number of points.
            if (nFilterPoints == 0)
                return data;
            else if (nFilterPoints == 3)
                return filterData3(data);
            else
                return filterData5(data);
        }

        //This needs to be replaced with a proper general nPoints moving average filter
        //that takes into account the special characteristics of the touch surface signal
        //and the possibility of a small number of points.
        private static double[] filterData3(double[] data)
        {
            if(data.Length<=1)
                return data;
            double[] fd = new double[data.Length];
            fd[0] = (data[0]+data[1])/2;
            int i;
            for (i = 1; i < (data.Length-1); i++)
            {
                fd[i] = (data[i - 1] + data[i] + data[i + 1]) / 3;
            }
            fd[i] = (data[i - 1] + data[i]) / 2;

            return fd;
        }

        //This needs to be replaced with a proper general nPoints moving average filter
        //that takes into account the special characteristics of the touch surface signal
        //and the possibility of a small number of points.
        private static double[] filterData5(double[] data)
        {
            if (data.Length <= 4)
                return data;
            double[] fd = new double[data.Length];
            fd[0] =  (data[0] + data[1] + data[2]) / 3;
            fd[1] =  (data[0] + data[1] + data[2] + data[3]) / 4;
            int i;
            for (i = 2; i < (data.Length - 2); i++)
            {
                fd[i] = (data[i - 2] + data[i - 1] + data[i] + data[i + 1] + data[i + 2]) / 5;
            }
            fd[i] = (data[i - 2] + data[i - 1] + data[i]+data[i+1]) / 4;
            fd[i + 1] = (data[i - 1] + data[i] + data[i + 1]) / 3;

            return fd;
        }


        public double getCorrelation(double[] d1, double[] d2)
        {
            double  r12 = 0;
            double  d1Av, d2Av, d1Sum, d2Sum;
            double  sumNum,         //sum numerator sum((d1-d1Av).(d2-d2Av))
                    sumD1DeltaSqr,  //sum(d1-d1Av)2
                    sumD2DeltaSqr;  //sum(d2-d2Av)2

            d1Av = d2Av = 0;
            d1Sum = d2Sum = 0;
            sumNum = sumD1DeltaSqr = sumD2DeltaSqr = 0;

            for (int i = 0; i < d1.Length; i++)
            {
                d1Sum += d1[i];
                d2Sum += d2[i];
            }
            d1Av = d1Sum / d1.Length;
            d2Av = d2Sum / d2.Length;

            double val, dif1, dif2, dif1Sqr, dif2Sqr;
            for (int i = 0; i < d1.Length; i++)
            {
                dif1 = d1[i] - d1Av;
                dif2 = d2[i] - d2Av;

                val = dif1 * dif2;
                sumNum += val;

                dif1Sqr = Math.Pow(dif1, 2);
                sumD1DeltaSqr += dif1Sqr;

                dif2Sqr = Math.Pow(dif2, 2);
                sumD2DeltaSqr += dif2Sqr;
            }
            r12 = sumNum / Math.Sqrt(sumD1DeltaSqr * sumD2DeltaSqr);

            return r12;
        }



        #region notNeededRotationSutff


        public static double[] getDeltaAngles(double[] x, double[] y, double xMax, double yMax)
        {
            double[] deltaAngles = new double[x.Length];
            double dx1, dx2;
            double dy1, dy2;
            deltaAngles[0] = 0;
            double dotProduct;
            double denom;
            double max = (yMax >= xMax) ? yMax : xMax;
            double scale = 100.0 / max;
            double threshold = 1 / max;
            double div;
            double mag1, mag2;

            dx1 = (x[1] - x[0]) * scale;
            dy1 = (y[1] - y[0]) * scale;

            for (int i = 1; i < (x.Length - 1); i++)
            {
                dx1 = (x[i] - x[i - 1]) * scale;
                dy1 = (y[i] - y[i - 1]) * scale;

                dx2 = (x[i + 1] - x[i]) * scale;
                dy2 = (y[i + 1] - y[i]) * scale;

                dotProduct = dx1 * dx2 + dy1 * dy2;
                mag1 = Math.Sqrt(dx1 * dx1 + dy1 * dy1);
                mag2 = Math.Sqrt(dx2 * dx2 + dy2 * dy2);
                denom = mag1 * mag2;
                div = dotProduct / denom;
                if (div > 1)
                    div = 1;
                else if (div < -1)
                    div = -1;
                deltaAngles[i] = Math.Acos(div) * 180 / Math.PI;
                if (deltaAngles[i] >= 179 && Math.Abs(deltaAngles[i - 1]) <= 1)
                    deltaAngles[i] = 0;
                if (double.IsNaN(deltaAngles[i]))
                    deltaAngles[i] = 0;

                ////normalize the value for comparisons
                dx1 = dx1 / mag1;
                dy1 = dy1 / mag1;
                dx2 = dx2 / mag2;
                dy2 = dy2 / mag2;

                //Need to double check the corrections below as they are making the results worse
                //if (dx1 > 0 && dy1 >= 0)
                //{//first quarter
                //    if ((dx2 >= dx1) ||
                //        (Math.Abs(dx2) < Math.Abs(dx1) && dy2 < dy1))
                //        deltaAngles[i] *= -1;
                //}
                //else if (dx1 <= 0 && dy1 > 0)
                //{//second quarter
                //    if ((dy2 >= dy1) ||
                //        (dx2 > dx1 && Math.Abs(dy2) < Math.Abs(dy1)))
                //        deltaAngles[i] *= -1;
                //}
                //else if (dx1 < 0 && dy1 <= 0)
                //{ //3rd quarter
                //    if ((dx2 <= dx1) ||
                //        (Math.Abs(dx2) < Math.Abs(dx1) && dy2 > dy1))
                //        deltaAngles[i] *= -1;
                //}
                //else
                //{//4th quarter
                //    if ((dy2 <= dy1) ||
                //        (dx2 < dx1 && Math.Abs(dy2) < Math.Abs(dy1)))
                //        deltaAngles[i] *= -1;
                //}


            }
            //deltaAngles[x.Length - 1] = 0;
            return deltaAngles;
        }


        public double guesseOrientationCalibrationAngle(TouchGesture tg, IMUGesture imug, int delay)
        {
            if (tg.gestureData.Count < 2)
                return 0;
            double[] xs;
            double[] ys;
            double[] actualTimes;
            double[] resampledTimes;
            double[] imuLinXVals = new double[tg.gestureData.Count];
            double[] imuLinYVals = new double[tg.gestureData.Count];
            double[] imuLinDVals = new double[tg.gestureData.Count];
            double[] actualIMUTimes = new double[tg.gestureData.Count];

            xs = new double[tg.gestureData.Count()];
            ys = new double[tg.gestureData.Count()];
            actualTimes = new double[xs.Length];
            int i, j;
            TouchGesPoint tp;
            if ((tg.gestureData.Count + 30 + delay) > imug.gestureData.Count)
                return 0;
            for (i = 0, j = 30 + delay; i < tg.gestureData.Count; i++, j++)
            {
                tp = tg.gestureData[i];
                xs[i] = tp.x;
                ys[i] = tp.y;
                actualTimes[i] = tp.actualTime;
                actualIMUTimes[i] = imug.gestureData[j].actualTime;
                imuLinXVals[i] = imug.gestureData[j].linearAcc[0];
                imuLinYVals[i] = imug.gestureData[j].linearAcc[1];
            }

            double samplingInterval;
            xs = resample(xs, actualTimes, out resampledTimes, out samplingInterval);
            ys = resample(ys, actualTimes, out resampledTimes, out samplingInterval);
            xs = filterData5(xs);
            ys = filterData5(ys);

            imuLinXVals = resample(imuLinXVals, actualIMUTimes, samplingInterval);
            imuLinYVals = resample(imuLinYVals, actualIMUTimes, samplingInterval);
            imuLinXVals = filterData5(imuLinXVals);
            imuLinYVals = filterData5(imuLinYVals);

            int n;
            if (xs.Length >= 10)
                n = 10;
            else
                n = xs.Length;
            double imuB1 = getRegressionB1(imuLinXVals, imuLinYVals, n);
            double tB1 = getRegressionB1(xs, ys, n);
            double imuAngle = Math.Atan2(imuB1, 1) * 180 / Math.PI;
            double tAngle = Math.Atan2(tB1, 1) * 180 / Math.PI;
            double dAngle = imuAngle - tAngle;
            if (Double.IsNaN(dAngle))
                dAngle = 0;
            if (dAngle < 0)
                dAngle += 360;
            return dAngle;
        }

        private double getRegressionB1(double[] xs, double[] ys, int n)
        {
            double[] xy = new double[n];
            double[] xSqr = new double[n];
            double sumX = 0,
                    sumY = 0;
            double sumXY = 0,
                    sumXSqr = 0;
            double Sxy, Sxx;
            for (int i = 0; i < n; i++)
            {
                sumX += xs[i];
                sumY += ys[i];
                sumXY += xs[i] * ys[i];
                xSqr[i] += xs[i] * xs[i];
                sumXSqr += xSqr[i];
            }
            Sxy = sumXY - (sumX * sumY) / n;
            Sxx = sumXSqr - (sumX * sumX) / n;
            double B1 = 0;
            if (Sxx != 0)
                B1 = Sxy / Sxx;
            return B1;
        }

        public double calculateOrientationCalibrationAngle(TouchGesture touchGesture, IMUGesture matchingIMUGesture, int delay)
        {
            int filterVal = 3;
            double bestAngle = 0;
            double bestCorX = -2, bestCorY = -2, bestCorAngle = -2;
            double corX = 0, corY = 0, corD = 0, corAngle = 0;
            for (int angle = 0; angle < 360; angle += 5)
            {
                getCorrelationFromRawGestureData(touchGesture, matchingIMUGesture, out corX, out corY, out corD, out corAngle, new RotateTransform(angle), delay, filterVal, false);
                if ((corX + corY) / 2 > (bestCorX + bestCorY) / 2)
                {
                    bestAngle = angle;
                    bestCorX = corX;
                    bestCorY = corY;
                    bestCorAngle = corAngle;
                }
            }
            getCorrelationFromRawGestureData(touchGesture, matchingIMUGesture, out corX, out corY, out corD, out corAngle, new RotateTransform(bestAngle), delay, filterVal, true);
            return bestAngle;
        }

        #endregion


    }
}


