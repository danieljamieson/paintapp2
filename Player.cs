﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Media;
using System.IO.Ports;
using System.Windows.Controls;
using System.Windows.Shapes;
using System.ComponentModel;
using Microsoft.Surface.Presentation.Controls;

namespace PaintApp1
{
    public class Player
    {
        private int playerIndex;                               // The index of the Player instance (0 or 1)
        public Image colorWheel;                               // The image representing the player's colour selection wheel
        public Ellipse wheelCenter;                            // The centre of the player's wheel (and therefore the currently selected colour) 
        public Color mostRecentColor;                          // The most recent color selected
        public Object mostRecentAction;                        // The most recent canvas action (for undo operations)
        public Double wheelAngle;                              // The current angle of the color wheel
        public Double touchMovePosition, touchLastPosition;    // Stored touch positions for the color wheel
        public Boolean selectingColor;                         // Is the user selecting a color?

        public SurfaceButton[] toolArray;                      // Array of available tools

        public CWASensor cwa;

        public Player(Image colorWheel, Ellipse wheelCenter)
        {
            this.colorWheel = colorWheel;
            this.wheelCenter = wheelCenter;

            mostRecentColor = Colors.Gray;

            playerIndex = int.Parse(colorWheel.Name.Substring(colorWheel.Name.Length - 1, 1));
        }

        public void WheelPointDown(double x, double y)
        {
            selectingColor = true;
            if (playerIndex == 0)
                touchLastPosition = x + y;
            else if (playerIndex == 1)
                touchLastPosition = x - y;
        }

        public void WheelPointUp(Color color)
        {
            selectingColor = false;
            wheelCenter.Fill = new SolidColorBrush(color);
            mostRecentColor = color;
        }

        public void WheelPointMove(double x, double y)
        {
            if (selectingColor)
            {
                if (playerIndex == 0)
                    touchMovePosition = x + y;
                else if (playerIndex == 1)
                    touchMovePosition = x - y;
                RotateWheel(touchMovePosition);
            }
        }

        // Rotate the color wheel based on the touch position
        private void RotateWheel(double touchMovePosition)
        {
            // If the wheel has rotated fully then reset angle
            if ((wheelAngle >= 360) || (wheelAngle <= -360))
            {
                wheelAngle = 0; // Set wheel angle to 0
            }

            // Update the wheel angle
            wheelAngle += ((touchLastPosition - touchMovePosition) / 2);

            // Update the last touch position
            touchLastPosition = touchMovePosition;

            // Perform color wheel rotation
            RotateTransform rotateTransformWheel = new RotateTransform(wheelAngle, 205, 205);
            colorWheel.RenderTransform = rotateTransformWheel;
        }

        public CWASensor startPort(string comPort)
        {
            cwa = new CWASensor(comPort);
            return cwa;
        }
    }
}
