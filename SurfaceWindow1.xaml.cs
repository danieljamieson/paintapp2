using System;
using System.IO;
using System.Collections.Generic;
using System.Collections;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;
using Microsoft.Surface;
using Microsoft.Surface.Presentation;
using Microsoft.Surface.Presentation.Automation;
using Microsoft.Surface.Presentation.Common;
using Microsoft.Surface.Presentation.Controls;
using Microsoft.Surface.Presentation.Generic;
using Microsoft.Surface.Presentation.Input;
using Microsoft.Surface.Presentation.Palettes;
using System.IO.Ports;
using System.Text.RegularExpressions;
using System.ComponentModel;
using SMARTTableSDK.Core;

namespace PaintApp1
{
    // Interaction logic for SurfaceWindow1.xaml
    public partial class SurfaceWindow1 : Window
    {
        public static SurfaceWindow1 selfRef;
        Stack<Stroke> strokeStack = new Stack<Stroke>();
        Stack<Object> actionsStack = new Stack<Object>();       // A stack containing the canvas actions
        Stack<Object> redoStack = new Stack<Object>();          // A stack of undo items for redo operations
        private Color mostRecentColor;                          // The most recent color selected
        private Object mostRecentAction;                        // The most recent canvas action (for undo operations)
        //private Double wheelAngle;                              // The current angle of the color wheel
        //private Double touchMovePosition;    // Stored touch positions for the color wheel
        private Double touchLastPosition;
        //private SurfaceButton[] toolArray;                      // Array of available tools
        private Boolean selectingColor;                         // Is the user selecting a color?
        //private Regex rx;                                       // Regex class used for parsing device data
        //private SerialPort port;                                // Serial port for Bluetooth connection
        //private SlidingWindow window;                           // Maintains the sliding window
        //private int count;                                      // The number of data points
        private readonly BackgroundWorker portWorker = new BackgroundWorker(); // Performs connection work asynchronously

        private Color defaultColour = Colors.Gray;
        private LoggingThread lgThread;
        const double A_DIV = 4096.0;    // Divide accelerometer data to get correct units
        const double G_MUL = 0.07;      // Multiply gyroscope data to get correct units
        const int WINDOW_SIZE = 64;     // Sliding window size
        const string COM_PORT = "COM3"; // Bluetooth serial port number
        const string DATA_REG = @"(?<ax>-?\d+),(?<ay>-?\d+),(?<az>-?\d+),(?<gx>-?\d+),(?<gy>-?\d+),(?<gz>-?\d+)";

        private Player[] players = new Player[2];
        public static long startTime;

        // Number of players in the game (will eventually be selected through UI)
        public static int SENSOR_COUNT;
        public string[] comPorts;

        public Analyzer analyzer;

        private int touches = 0;
        public double dpiX, dpiY;

        public static PresentationSource source;

        // Default constructor
        public SurfaceWindow1()
        {
            InitializeComponent();
            selfRef = this;

            players[0] = new Player(colorWheel0, wheelCenter0);
            players[1] = new Player(colorWheel1, wheelCenter1);

            analyzer = new Analyzer(this);

            // Set up Logging Thread first ready to start sampling when IMUs have connected
            lgThread = new LoggingThread(this, analyzer);
         
            // Set the canvas defaults
            InitializeCanvasWithDefaults();
        }

        private void SurfaceWindow1_Loaded(object sender, RoutedEventArgs e)
        {
            //#if SUPPORT_SMART_TABLE
            //Register Smart table SDK
            TableSDK.Instance.Register(this);
            //#endif
            source = PresentationSource.FromVisual(this);

            if (source != null)
            {
                dpiX = (1232.6 / 10.1) * source.CompositionTarget.TransformToDevice.M11;
                dpiY = (734.7 / 5.65) * source.CompositionTarget.TransformToDevice.M22;
            }
        }

        private void SurfaceWindow_SizeChanged(object sender, SizeChangedEventArgs e)
        {
            Size size = e.NewSize;
            //Console.WriteLine("sX: " + size.Width + ", sY: " + size.Height);
            if (source != null)
            {
                dpiX = (1232.6 / 10.1) * source.CompositionTarget.TransformToDevice.M11;
                dpiY = (734.7 / 5.65) * source.CompositionTarget.TransformToDevice.M22;
            }
        }

        protected override void OnInitialized(EventArgs e)
        {
            base.OnInitialized(e);

            // Initilize fields and background workers
            //InitializeFields();
            //InitializeBackgroundWorkers();
        }

        /// <summary>
        /// Initialise field objects and events
        /// </summary>
        private void InitializeFields()
        {
            //this.recogniser = new Recogniser();
            //this.rx = new Regex(DATA_REG);
            //this.window = new SlidingWindow(WINDOW_SIZE);
            //this.count = 0;
        }

        // Occurs when the window is about to close
        protected override void OnClosed(EventArgs e)
        {
            // Try to close Bluetooth port
            try
            {
                foreach (Player p in players)
                {
                    if (p.cwa != null && p.cwa.getPort().IsOpen) {
                        p.cwa.getPort().WriteLine("stream=0\r\n");
                        p.cwa.getPort().Close();
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Exception: " + ex.ToString());
            }

            base.OnClosed(e);
        }

        // Set the canvas defaults
        private void InitializeCanvasWithDefaults()
        {
            players[0].toolArray = new SurfaceButton[] { brushButton0, pencilButton0, eraserButton0 }; // Add the tools to the available tool array
            players[1].toolArray = new SurfaceButton[] { brushButton1, pencilButton1, eraserButton1 }; // Add the tools to the available tool array

            mainCanvas.DefaultDrawingAttributes.Color = Colors.Black; // Set stroke colour to black
            mostRecentColor = Colors.Black; // Set recent color to black
            // mainCanvas.UsesTouchShape = false; // Prevent brush from using touch shape
            SetTool(players[0].toolArray[1], players[0].toolArray, 5); // Set the pencil as the initial tool
            SetTool(players[1].toolArray[1], players[1].toolArray, 5); // for both players
        }

        // Switch to the pencil tool
        private void SwitchToolPencil(object sender, RoutedEventArgs e)
        {
            // Get the index using the button that was pressed
            SurfaceButton pencilButton = sender as SurfaceButton;
            int playerIndex = int.Parse(pencilButton.Name.Substring(pencilButton.Name.Length - 1, 1));

            SetTool(pencilButton, players[playerIndex].toolArray, 5);
        }

        // Switch to the brush tool
        private void SwitchToolBrush(object sender, RoutedEventArgs e)
        {
            // Get the index using the button that was pressed
            SurfaceButton brushButton = sender as SurfaceButton;
            int playerIndex = int.Parse(brushButton.Name.Substring(brushButton.Name.Length - 1, 1));

            SetTool(brushButton, players[playerIndex].toolArray, 20);
        }

        // Switch to the eraser tool
        private void SwitchToolEraser(object sender, RoutedEventArgs e)
        {
            // Get the index using the button that was pressed
            SurfaceButton eraserButton = sender as SurfaceButton;
            int playerIndex = int.Parse(eraserButton.Name.Substring(eraserButton.Name.Length - 1, 1));

            SetTool(eraserButton, players[playerIndex].toolArray, 20, true);
        }

        // Set the stroke color
        private void SetStrokeColor(Color strokeColor)
        {
            //mainCanvas.DefaultDrawingAttributes.Color = defaultColour;
        }

        // Set the stroke colour of a specific action
        private void SetActionStrokeColor(Object action, Color strokeColor)
        {
            //mainCanvas.DefaultDrawingAttributes.Color = strokeColor;
        }

        // Set the stroke size
        private void SetStrokeSize(int strokeSize)
        {
            //mainCanvas.DefaultDrawingAttributes.Height = strokeSize; // Set the height attribute
            //mainCanvas.DefaultDrawingAttributes.Width = strokeSize; // Set the width attribute
        }

        // Set the current tool and stroke size
        private void SetTool(SurfaceButton showTool, SurfaceButton[] toolArray, int strokeSize, Boolean isEraser = false)
        {
            // Get the index using the button that was pressed
            int playerIndex = int.Parse(showTool.Name.Substring(showTool.Name.Length - 1, 1));

            SetStrokeSize(strokeSize); // Set the stroke size

            // If the tool is the eraser then hide the color wheel and set stroke color to white
            if (isEraser)
            {
                SetStrokeColor(Colors.White);
                players[playerIndex].colorWheel.Visibility = Visibility.Hidden;
                players[playerIndex].wheelCenter.Visibility = Visibility.Hidden;
            }
            else
            {
                SetStrokeColor(mostRecentColor); // Set the brush color to the most recently selected
                players[playerIndex].colorWheel.Visibility = Visibility.Visible;
                players[playerIndex].wheelCenter.Visibility = Visibility.Visible;
            }

            // Loop through the tool array and position accordingly
            foreach (SurfaceButton tool in toolArray) 
            {
                if (tool != showTool)
                {
                    // Push the tool down
                    Thickness hideMargin = tool.Margin;
                    hideMargin.Bottom = -30;
                    tool.Margin = hideMargin;
                }
                else
                {
                    // Pop the tool up
                    Thickness showMargin = showTool.Margin;
                    showMargin.Bottom = 0;
                    showTool.Margin = showMargin;
                }
            }
            
        }

        // Undo the last stroke
        private void UndoStroke(object sender, RoutedEventArgs e)
        {
            // If there is a stroke on the stack then pop it off and remove it from the canvas
            if (actionsStack.Count > 0)
            {
                mostRecentAction = actionsStack.Pop(); // Pop the action from the stack

                redoStack.Push(mostRecentAction); // Add the action to the redo stack

                if (mostRecentAction is Stroke)
                {
                    Stroke editAction = (Stroke)mostRecentAction;
                    mainCanvas.Strokes.Remove(editAction); // Remove the stroke from the canvas
                }

                if (mostRecentAction is Polygon)
                {
                    Polygon editAction = (Polygon)mostRecentAction;
                    //mainCanvas.Children.Remove(editAction); // Remove the action from the canvas
                }

            }
        }

        // Redo the last undo action
        private void RedoStroke(object sender, RoutedEventArgs e)
        {
            // If there is a stroke on the stack then pop it off and add it to the canvas
            if (redoStack.Count > 0)
            {
                mostRecentAction = redoStack.Pop(); // Pop the action from the stack

                actionsStack.Push(mostRecentAction); // Add the action to the stack

                if (mostRecentAction is Stroke)
                {
                    Stroke editAction = (Stroke)mostRecentAction;
                    
                    mainCanvas.Strokes.Add(editAction); // Remove the stroke from the canvas
                }

                if (mostRecentAction is Polygon)
                {
                    Polygon editAction = (Polygon)mostRecentAction;
                    // Children mainCanvas.Children.Add(editAction); // Remove the action from the canvas
                }

            }
        }

        private void canvas_TouchDown(object sender, TouchEventArgs e)
        {
            touches++; // effects the identification
            lgThread.touchDown(e.TouchDevice, mainCanvas.Strokes.Count);
        }

        private void canvas_TouchUp(object sender, TouchEventArgs e)
        {
            lgThread.touchUp(e.TouchDevice);
            touches--; // effects the identification
        }

        // Add new strokes to the top of the action stack
        private void OnStrokeCollected(object sender, InkCanvasStrokeCollectedEventArgs args)
        {
            actionsStack.Push(args.Stroke);
        }

        #region Identification

        /// <summary>
        /// Takes a touch gesture and compares it with the stream coming in from each IMU connected
        /// </summary>
        /// <param name="tg">The touch gesture to compare</param>
        /// <returns>The output from the correlation algorithm for each TouchGesture-IMUGesture comparison</returns>
        private List<GestureMatchingInfo> getBestMatch(TouchGesture tg)
        {
            //cmp.Show();
            int sensorCount = lgThread.cwas.Count;
            IMUGesture imug;

            int extraReadings = 0;
            int filterVal = 5;
            bool resampleData = false;

            double delay = 0, angle = 0, corX = 0, corY = 0, corD = 0;
            bool drawTouch = false;
            List<GestureMatchingInfo> matches = new List<GestureMatchingInfo>();
            for (int i = 0; i < sensorCount; i++)
            {
                imug = lgThread.RetrieveIMUGestureByIndex(i);
                imug.indexOfGestureInList = tg.indexOfGestureInList[i];
                delay = analyzer.guessOptimiumDelayFix(tg, imug, extraReadings, filterVal, resampleData);

                if (i == 0) drawTouch = true;
                else drawTouch = false;

                angle = 0;
                analyzer.getCorrelationFromRawGestureData(tg, imug, out corX, out corY, out corD, (int)delay, filterVal, resampleData, drawTouch, true);
                
                matches.Add(new GestureMatchingInfo(corD, i, angle, (int)delay));
            }
            return matches;
        }

        public void makeIdentification(TouchGesture strokeGesture)
        {
            try
            {
                //TouchGesture touchGesture = lgThread.activeStrokes[stroke];
                //TouchGesture strokeGesture = stylusPointCollectionToTouchGesture(stroke.StylusPoints);

                List<GestureMatchingInfo> matches = getBestMatch(strokeGesture);
                GestureMatchingInfo[] sortedList = GestureMatchingInfo.getSortedArray(matches);

                double maxCorrelation = double.MinValue;
                int identifiedSensorID = 0;
                for (int i = 0; i < sortedList.Length; i++)
                {
                    Console.WriteLine("Sensor " + i + ": " + matches[i].matchPercentage + ", delay: " + matches[i].delay);
                    if (matches[i].matchPercentage > maxCorrelation)
                    {
                        maxCorrelation = matches[i].matchPercentage;
                        identifiedSensorID = i;
                    }
                }
                if (strokeGesture.stroke != null) strokeGesture.stroke.DrawingAttributes.Color = players[identifiedSensorID].mostRecentColor;
            }
            catch (KeyNotFoundException e)
            {
                //throw e;
                Console.WriteLine("KeyNotFoundException caught: ignoring");
                Console.WriteLine(e.StackTrace);
            }
        }

        public void makeOnlineIdentification(TouchDevice[] touchKeys, double[][] corrD, int[] signalLengths)
        {
            double threshold = 0.623 ;
            // i specifies the touch
            for (int i = 0; i < corrD.Length; i++)
            {
                double maxCorr = double.MinValue;
                int identifiedSensorID = 0;
                // j specifies the sensor
                for (int j = 0; j < corrD[i].Length; j++)
                {
                    Console.WriteLine(i + ", " + j + ": " + corrD[i][j]);
                    if (corrD[i][j] > maxCorr) {
                        maxCorr = corrD[i][j];
                        identifiedSensorID = j;
                    }
                }
                if (signalLengths[i] < 150)
                {
                    threshold = 0.9;
                }
                if (maxCorr > threshold)
                {
                    Console.WriteLine("identified touch as sensor " + identifiedSensorID);
                    identifyStroke(touchKeys[i], identifiedSensorID);
                }
            }
        }

        public void makeMagnitudeDiffBasedOnlineIdentification(TouchDevice[] touchKeys, double[][] corrD, int[] signalLengths)
        {
            double threshold = 30;
            // i specifies the touch
            for (int i = 0; i < corrD.Length; i++)
            {
                double minCorr = double.MaxValue;
                int identifiedSensorID = 0;

                double[] diff = new double[corrD[i].Length - 1];

                // j specifies the sensor
                for (int j = 0; j < corrD[i].Length; j++)
                {
                    if (corrD[i][j] < minCorr)
                    {
                        minCorr = corrD[i][j];
                        identifiedSensorID = j;
                    }
                }

                // now find the differences
                for (int k = 0, j = 0; k < diff.Length; k++, j++)
                {
                    if (corrD[i][j] >= corrD[i][j + 1])
                        diff[k] = corrD[i][j] - corrD[i][j + 1];
                    else if (corrD[i][j] < corrD[i][j + 1])
                        diff[k] = corrD[i][j + 1] - corrD[i][j];
                    Console.WriteLine("diff[" + k + "] = " + diff[k]);
                }
                if (allDifferencesAboveThreshold(diff) && signalLengths[i] > 30)
                {
                    Console.WriteLine("identified touch as sensor " + identifiedSensorID);
                    identifyStrokeMag(touchKeys[i], identifiedSensorID);
                }
              
            }
        }

        private bool allDifferencesAboveThreshold(double[] diff)
        {
            double threshold = 50;
            for (int i = 0; i < diff.Length; i++)
            {
                if (diff[i] < threshold)
                    return false;
            }
            return true;
        }

        public void identifyStrokeMag(TouchDevice device, int sensorID)
        {
            analyzer.touches[device].Identified();
            SurfaceInkCanvas canvas = device.Target as SurfaceInkCanvas;
            Stroke stroke;
            if (canvas.Strokes.Count - touches >= 0 && canvas.Strokes.Count - touches < canvas.Strokes.Count)
            {
                if (analyzer.strokeIndices.ContainsKey(device))
                {
                    int strokeIndex = analyzer.strokeIndices[device];
                    stroke = canvas.Strokes[strokeIndex];
                }
                else
                {
                    stroke = canvas.Strokes[canvas.Strokes.Count - touches];
                }
                analyzer.touches[device].Identified(); // mark as identified
                if (stroke != null) stroke.DrawingAttributes.Color = players[sensorID].mostRecentColor;
                else Console.WriteLine("could not retrieve stroke reference");
            }
        }

        public void identifyStroke(TouchDevice device, int sensorID)
        {
            SurfaceInkCanvas canvas = device.Target as SurfaceInkCanvas;
            Stroke stroke;
            if (canvas.Strokes.Count - touches >= 0 && canvas.Strokes.Count - touches < canvas.Strokes.Count)
            {
                if (analyzer.strokeIndices.ContainsKey(device))
                {
                    int strokeIndex = analyzer.strokeIndices[device];
                    stroke = canvas.Strokes[strokeIndex];
                }
                else
                {
                    stroke = canvas.Strokes[canvas.Strokes.Count - touches];
                }
                analyzer.touches[device].Identified(); // mark as identified
                if (stroke != null) stroke.DrawingAttributes.Color = players[sensorID].mostRecentColor;
            }
        }

        #endregion

        // Used to call the correct pixel color function
        private void GetColor(int playerIndex, bool isMouse, MouseEventArgs e, TouchEventArgs t)
        {
            Color selectedColor;

            if (isMouse)
            {
                selectedColor = GetPixelColor(e.Device, playerIndex);
            }
            else
            {
                selectedColor = GetPixelColor(t.Device, playerIndex);
            }

            // Update the color of the wheel center to indicate current color
            players[playerIndex].wheelCenter.Fill = new SolidColorBrush(selectedColor);

            // Update the most recently selected color
            mostRecentColor = selectedColor;
            players[playerIndex].mostRecentColor = selectedColor;
        }

        // Mouse moved within the color wheel
        private void WheelMouseMove(object sender, MouseEventArgs e)
        {
            if (selectingColor)
            {
                Point touch = e.GetPosition(this);
                //touchMovePosition = touch.X + touch.Y;

                //RotateWheel(touchMovePosition);
                int playerIndex = int.Parse((sender as Image).Name.Substring((sender as Image).Name.Length - 1, 1));
                players[playerIndex].WheelPointMove(touch.X, touch.Y);

                GetColor(playerIndex, true, e, null);
            }
        }

        // Touch moved within the color wheel
        private void WheelTouchMove(object sender, TouchEventArgs e)
        {
            if (selectingColor)
            {
                TouchPoint touch = e.GetTouchPoint(this);
                //touchMovePosition = touch.Position.X + touch.Position.Y;

                //RotateWheel(touchMovePosition);
                // Get the index using the name of the wheel that the user pressed down on
                int playerIndex = int.Parse((sender as Image).Name.Substring((sender as Image).Name.Length - 1, 1));
                players[playerIndex].WheelPointMove(touch.Position.X, touch.Position.Y);

                GetColor(playerIndex, false, null, e);
            }
        }

        // Mouse down within the color wheel
        private void WheelMouseDown(object sender, MouseButtonEventArgs e)
        {
            selectingColor = true;
            Point touch = e.GetPosition(this);
            touchLastPosition = touch.X + touch.Y;

            // Get the index using the name of the wheel that the user pressed down on
            int playerIndex = int.Parse((sender as Image).Name.Substring((sender as Image).Name.Length - 1, 1));
            players[playerIndex].WheelPointDown(touch.X, touch.Y);
        }

        // Touch down within the color wheel
        private void WheelTouchDown(object sender, TouchEventArgs e)
        {
            selectingColor = true; 
            TouchPoint touch = e.GetTouchPoint(this);
            touchLastPosition = touch.Position.X + touch.Position.Y; // this line has had it's functionality replaced

            // Get the index using the name of the wheel that the user pressed down on
            int playerIndex = int.Parse((sender as Image).Name.Substring((sender as Image).Name.Length - 1, 1));
            players[playerIndex].WheelPointDown(touch.Position.X, touch.Position.Y);
        }

        // Mouse up within the color wheel
        private void WheelMouseUp(object sender, MouseButtonEventArgs e)
        {
            selectingColor = false;
            // Get the index using the name of the wheel that the user pressed down on
            int playerIndex = int.Parse((sender as Image).Name.Substring((sender as Image).Name.Length - 1, 1));
            
            Color color = GetPixelColor(e.Device, playerIndex);
            //mainCanvas.DefaultDrawingAttributes.Color = color;
            //players[0].wheelCenter.Fill = new SolidColorBrush(color); // this line has had it's functionality replaced
            mostRecentColor = color; // this line has had it's functionality replaced

            players[playerIndex].WheelPointUp(color);
        }

        // Touch up within the color wheel
        private void WheelTouchUp(object sender, TouchEventArgs e)
        {
            selectingColor = false;
            // Get the index using the name of the wheel that the user pressed down on
            int playerIndex = int.Parse((sender as Image).Name.Substring((sender as Image).Name.Length - 1, 1));
            
            Color color = GetPixelColor(e.Device, playerIndex);
            //mainCanvas.DefaultDrawingAttributes.Color = color;
            //players[0].wheelCenter.Fill = new SolidColorBrush(color); // this line has had it's functionality replaced
            mostRecentColor = color; // this line has had it's functionality replaced

            players[playerIndex].WheelPointUp(color);
        }

        // Gets the color of a specific pixel
        private System.Windows.Media.Color GetPixelColor(InputDevice inputDevice, int playerIndex)
        {
            try
            {
                // Translate the input point to bitmap coordinates
                Point bitmapPoint = inputDevice.GetPosition(players[playerIndex].colorWheel);

                // The point is outside the colorwheel. Return current color.
                if (bitmapPoint.X < 0 || bitmapPoint.X >= players[playerIndex].colorWheel.Source.Width || bitmapPoint.Y < 0 || bitmapPoint.Y >= players[playerIndex].colorWheel.Source.Height)
                {
                    return Colors.Red; // mainCanvas.DefaultDrawingAttributes.Color;
                }

                // The point is within the color wheel, return the color at this point
                CroppedBitmap cb = new CroppedBitmap(players[playerIndex].colorWheel.Source as BitmapSource, new Int32Rect((int)bitmapPoint.X, (int)bitmapPoint.Y, 1, 1));
                byte[] pixels = new byte[4];
                cb.CopyPixels(pixels, 4, 0);

                Color pixelColor = Color.FromRgb(pixels[2], pixels[1], pixels[0]);

                // If the pixel color is black or white then return current color
                if ((pixelColor != Colors.Black) || (pixelColor != Colors.White))
                {
                    return Color.FromRgb(pixels[2], pixels[1], pixels[0]);
                }
                else
                {
                    return Colors.Red;// mainCanvas.DefaultDrawingAttributes.Color;
                }
            }
            catch
            {
                // Failed, return current color.
                return Colors.Red;// mainCanvas.DefaultDrawingAttributes.Color;
            }
        }

        // Fill the last stroke with paint
        private void FillLast(object sender, RoutedEventArgs e)
        {
            if (actionsStack.Count > 0)
            {
                Object recent = actionsStack.Pop();
                actionsStack.Push(recent);

                if (recent is Stroke)
                {
                    Stroke recentStroke = (Stroke)recent;

                    // Create a Polygon
                    Polygon fillPolygon = new Polygon();
                    fillPolygon.Fill = new SolidColorBrush(mostRecentColor);

                    PointCollection polygonPoints = new PointCollection();

                    // Loop through the stylus points
                    foreach (StylusPoint stylusPoint in recentStroke.StylusPoints)
                    {
                        // Create a point and add to selection
                        System.Windows.Point Point1 = new System.Windows.Point(stylusPoint.X, stylusPoint.Y);
                        polygonPoints.Add(Point1);
                    }

                    // Set Polygon.Points properties
                    fillPolygon.Points = polygonPoints;

                    // Add Polygon to the page
                    // Children mainCanvas.Children.Add(fillPolygon);
                    actionsStack.Push(fillPolygon);
                }
            }
        }

        // Clear all strokes from the canvas
        private void ClearCanvas(object sender, RoutedEventArgs e)
        {
            mainCanvas.Strokes.Clear();
            // Children mainCanvas.Children.Clear();

            actionsStack.Clear();
            redoStack.Clear();
        }

        #region Button Click Functions

        private void settingsBtn_Click(object sender, RoutedEventArgs e)
        {
            Settings stsWindow = new Settings(this);
            stsWindow.ShowDialog();
        }

        private void connectBtn_Click(object sender, RoutedEventArgs e)
        {
            if (comPorts != null)
            {
                //TableSDK.Instance.Register(this);
                //Match m = Regex.Match(comPorts, pattern, RegexOptions.IgnoreCase);
                // remove the menu buttons
                menuPanel.Visibility = Visibility.Hidden;
                menuPanel.IsEnabled = false;

                // for each com port that the user entered, create a new player
                //players = new List<Player>();
                for (int i = 0; i < comPorts.Length; i++)
                {
                    SENSOR_COUNT++;
                    comPorts[i].ToUpper();
                    if (i < 2) lgThread.cwas.Add(players[i].startPort(comPorts[i]), new IMUGesture());
                }

                startTime = DateTime.Now.Ticks / LoggingThread.TIMESCALE;
                lgThread.StartLogging();
                SENSOR_COUNT = comPorts.Length;
                analyzer.initialiseOnline(SENSOR_COUNT, 5);
                Start();
            }
        }

        private void Start()
        {
            SurfaceInkCanvas surf = new SurfaceInkCanvas();
            mainCanvas.Strokes.Clear();
            mainCanvas.PreviewTouchDown += canvas_TouchDown;
            mainCanvas.PreviewTouchUp   += canvas_TouchUp;
            mainCanvas.StrokeCollected  += OnStrokeCollected;
        }

        #endregion
    }

    /// <summary>
    ///  Data point - stores X, Y and Z components
    /// </summary>
    public struct DataPoint
    {
        public double x, y, z;
        public int count;

        public DataPoint(double x, double y, double z, int count)
        {
            this.x = x;
            this.y = y;
            this.z = z;
            this.count = count;
        }

        public override string ToString()
        {
            return string.Format("{0}, {1}, {2} at {3}", x, y, z, count);
        }
    }
}