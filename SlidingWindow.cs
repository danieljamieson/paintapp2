﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections.Concurrent;
using System.Diagnostics;

namespace PaintApp1
{
    /// <summary>
    ///     Maintains sliding window for activity recognition
    ///     and performs several l
    /// </summary>
    class SlidingWindow
    {
        private LinkedList<DataPoint> accelWindow;
        private LinkedList<DataPoint> gyroWindow;

        public int Size { get; set; }

        const double ACCEL_Y_THRESHOLD = 2.2;

        /// <summary>
        /// Sliding window constructor
        /// </summary>
        /// <param name="size">Window size</param>
        /// <param name="inc">TODO: window increment size</param>
        public SlidingWindow(int size)
        {
            this.Size = size;
            accelWindow = new LinkedList<DataPoint>();
            gyroWindow = new LinkedList<DataPoint>();
        }

        /// <summary>
        /// Updates the current window
        /// </summary>
        /// <param name="accelP">Accelerometer point</param>
        /// <param name="gyroP">Gyroscope point</param>
        public void UpdateWindow(DataPoint accelP, DataPoint gyroP)
        {
            accelWindow.AddLast(accelP);
            gyroWindow.AddLast(gyroP);

            if (accelWindow.Count > Size)
            {
                accelWindow.RemoveFirst();
            }

            if (gyroWindow.Count > Size)
            {
                gyroWindow.RemoveFirst();
            }
        }

        /// <summary>
        /// Calculates standard deviation of accelerometer window data
        /// </summary>
        /// <returns>Standard deviation of accelerometer data</returns>
        public double AccelStandardDeviation()
        {
            if (accelWindow.Count > 1)
            {
                double mean = AccelMean();
                double sum = 0;
                foreach (DataPoint p in accelWindow)
                {
                    double mag = Magnitude(p);
                    sum += ((mag - mean) * (mag - mean));
                }

                return Math.Sqrt(sum / (accelWindow.Count - 1));
            }

            return 0;
        }

        /// <summary>
        /// Calculates the magnitude of a point's X and Z components
        /// </summary>
        /// <param name="p">Point to compute</param>
        /// <returns>Magnitude of X and Z components</returns>
        public double XZMagnitude(DataPoint p)
        {
            return Math.Sqrt((p.x * p.x) + (p.z * p.z));
        }

        /// <summary>
        /// Calculates standard deviation of gyroscope window data
        /// </summary>
        /// <returns>Standard deviation of gyroscope data</returns>
        public double GyroStandardDeviation()
        {
            if (gyroWindow.Count > 1)
            {
                double mean = GyroMean();
                double sum = 0;
                foreach (DataPoint p in gyroWindow)
                {
                    double mag = Magnitude(p);
                    sum += ((mag - mean) * (mag - mean));
                }

                return Math.Sqrt(sum / (gyroWindow.Count - 1));
            }
            else
            {
                throw new DivideByZeroException();
            }
        }

        /// <summary>
        /// Calculates where the XY magnitude of the accel. window points exceeds a threshold
        /// </summary>
        /// <param name="thresh">Arbitrary threshold</param>
        /// <returns>Position where the point exceeds the threshold</returns>
        public int ExceedsXYMagAccelThreshold(double thresh)
        {
            foreach (DataPoint p in accelWindow)
            {
                if (XZMagnitude(p) > thresh)
                {
                    return p.count;
                }
            }

            return -1;
        }

        /// <summary>
        /// Calculates where the Y value of the gyro. window data exceeds a threshold
        /// </summary>
        /// <param name="thresh">Arbitrary threshold</param>
        /// <returns>Position where the point exceeds the threshold</returns>
        public int ExceedsYGyroThreshold(int thresh)
        {
            foreach (DataPoint p in gyroWindow)
            {
                if (Math.Abs(p.y) > thresh)
                {
                    return p.count;
                }
            }

            return -1;
        }

        /// <summary>
        /// Calculates the range of the accelerometer magnitudes in the current window
        /// </summary>
        /// <returns>Range of the accelerometer magnitudes</returns>
        public double AccelRange()
        {
            double hMag = 0;
            double lMag = 0;

            foreach (DataPoint p in accelWindow)
            {
                double pMag = Magnitude(p);
                if (pMag > hMag)
                {
                    hMag = pMag;
                }
                else if (pMag < lMag)
                {
                    lMag = pMag;
                }
            }

            return hMag - lMag;
        }

        /// <summary>
        /// Computes the kurtosis of the accelerometer window data
        /// </summary>
        /// <returns>Kurtosis of the accelerometer window data</returns>
        public double AccelKurtosis()
        {
            double num = 0;
            double den = (accelWindow.Count - 1) * Math.Pow(AccelStandardDeviation(), 4);
            foreach (DataPoint p in accelWindow)
            {
                double magP = Magnitude(p);
                num += Math.Pow(magP - AccelMean(), 4);
            }

            return num / den;
        }

        /// <summary>
        /// Finds the number of large, Y-component peaks in the accelerometer window data
        /// </summary>
        /// <returns>Number of Y-component accelerometer peaks</returns>
        public int AccelYPeaks() 
        {
            int peaks = 0;

            foreach (DataPoint p in accelWindow)
            {
                if (p.y > ACCEL_Y_THRESHOLD)
                {
                    peaks++;
                }
            }

            return peaks;
        }

        /// <summary>
        /// Computes the kurtosis of the gyroscope window data
        /// </summary>
        /// <returns>Kurtosis of gyroscope window data</returns>
        public double GyroKurtosis()
        {
            return 0;
        }

        /// <summary>
        /// Computes the mean of accelerometer window data
        /// </summary>
        /// <returns>Mean of accelerometer window data</returns>
        public double AccelMean()
        {
            double sum = 0;
            foreach (DataPoint p in accelWindow)
            {
                sum += Magnitude(p);
            }

            return sum / accelWindow.Count;
        }

        /// <summary>
        /// Computes the mean of the gyro window data
        /// </summary>
        /// <returns>Mean of gyroscope window data</returns>
        public double GyroMean()
        {
            double sum = 0;
            foreach (DataPoint p in gyroWindow)
            {
                sum += Magnitude(p);
            }

            return sum / gyroWindow.Count;
        }

        /// <summary>
        /// Computes the magnitude of a point
        /// </summary>
        /// <param name="p">Point to compute</param>
        /// <returns>Magntitude of point</returns>
        private double Magnitude(DataPoint p)
        {
            return Math.Sqrt((p.x * p.x) + (p.y * p.y) + (p.z * p.z));
        }

        public int AccelSize()
        {
            return accelWindow.Count;
        }
    }
}
