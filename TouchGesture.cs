﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Windows;
using System.Windows.Input;
using System.Windows.Ink;

namespace PaintApp1
{
    public struct TouchGesPoint
    {
        public string gestureID;
        public int gestureNo;
        public double unifiedTime,
                    actualTime;
        public int x, y;
        //public double accX;
        //public double accY;

        public void print()
        {
            Console.WriteLine("x: " + x + ", y: " + y);
        }

        public static TouchGesPoint read(String str)
        {
            TouchGesPoint td = new TouchGesPoint();
            string[] words = str.Split(',');
            td.gestureID = words[0];
            td.gestureNo = Int32.Parse(words[1]);
            td.unifiedTime = long.Parse(words[2]);
            td.actualTime = long.Parse(words[3]);
            td.x = int.Parse(words[4]);
            td.y = int.Parse(words[5]);
            return td;
        }

        public static TouchGesPoint newPoint(double x, double y)
        {
            TouchGesPoint tp = new TouchGesPoint();
            tp.x = (int)x; tp.y = (int)y;
            return tp;
        }

        public static TouchGesPoint newPoint(double x, double y, long time)
        {
            TouchGesPoint tp = new TouchGesPoint();
            tp.x = (int)x; tp.y = (int)y;
            tp.actualTime = time;
            tp.unifiedTime = time;
            return tp;
        }
    }
    public class TouchGesture
    {
        public SurfaceWindow1 parentWindow;
        public InputDevice inputDevice;
        public int[] indexOfGestureInList;
        public string uniqueID; //gestureID+"-"+gestureNo;
        public string gestureID;
        public int gestureNo;
        public List<TouchGesPoint> gestureData;
        public bool isTouch;
        public long startTime;
        public int oldX, oldY;

        public Stroke stroke;

        public TouchGesture()
        {
            gestureData = new List<TouchGesPoint>();
        }
        public TouchGesture(SurfaceWindow1 parent)
        {
            parentWindow = parent;
            gestureData = new List<TouchGesPoint>();
        }
        public void addTouchPoint(TouchGesPoint tp) {
            gestureData.Add(tp);
        }
        public void specifyStroke(Stroke s) {
            stroke = s;
        }

        public double getRelativeTime(int i)
        {
            return gestureData[i].actualTime - gestureData[0].actualTime;
        }

        public void print()
        {
            Console.WriteLine("Printing touch gesture");
            foreach (TouchGesPoint tp in gestureData)
            {
                tp.print();
            }
        }

        public Point startGesture(InputDevice id, bool isTouch, long timeStamp)
        {
            gestureData = new List<TouchGesPoint>();
            inputDevice = id;
            this.isTouch = isTouch;
            startTime = timeStamp;// System.DateTime.Now.Ticks / 200000;
            if (isTouch)
            {
                oldX = (int)((TouchDevice)inputDevice).GetTouchPoint(parentWindow).Position.X;
                oldY = (int)((TouchDevice)inputDevice).GetTouchPoint(parentWindow).Position.Y;
            }
            else
            {
                oldX = (int)((MouseDevice)inputDevice).GetPosition(parentWindow).X;
                oldY = (int)((MouseDevice)inputDevice).GetPosition(parentWindow).Y;
            }
            Point pt = new Point(oldX, oldY);
            return pt;
        }

        public Point updateGesture(long time)
        {
            TouchGesPoint tgp = new TouchGesPoint();
            tgp.actualTime = time;
            int x = 0;
            int y = 0;
            if (isTouch)
            {
                x = (int)((TouchDevice)inputDevice).GetTouchPoint(parentWindow).Position.X;
                y = (int)((TouchDevice)inputDevice).GetTouchPoint(parentWindow).Position.Y;
            }
            else
            {
                x = (int)((MouseDevice)inputDevice).GetPosition(parentWindow).X;
                y = (int)((MouseDevice)inputDevice).GetPosition(parentWindow).Y;
            }
            tgp.x = x;
            tgp.y = y;
            Point pt = new Point(x, y);
            long dt = time - startTime;
            if (dt != 0)
            {
                gestureData.Add(tgp);
                startTime = time;
                oldX = x;
                oldY = y;
            }
            return pt;
        }

        public TouchGesPoint newPoint(StylusPoint spt)
        {
            TouchGesPoint pt = TouchGesPoint.newPoint(spt.X, spt.Y);
            return pt;
        }

        public StylusPoint startGestureStylus(InputDevice id, bool isTouch, long timeStamp)
        {
            gestureData = new List<TouchGesPoint>();
            inputDevice = id;
            this.isTouch = isTouch;
            startTime = timeStamp;// System.DateTime.Now.Ticks / 200000;
            if (isTouch)
            {
                oldX = (int)((TouchDevice)inputDevice).GetTouchPoint(parentWindow).Position.X;
                oldY = (int)((TouchDevice)inputDevice).GetTouchPoint(parentWindow).Position.Y;
            }
            else
            {
                oldX = (int)((MouseDevice)inputDevice).GetPosition(parentWindow).X;
                oldY = (int)((MouseDevice)inputDevice).GetPosition(parentWindow).Y;
            }
            StylusPoint pt = new StylusPoint(oldX, oldY);
            return pt;
        }

        public StylusPoint updateGestureStylus(long time)
        {
            TouchGesPoint tgp = new TouchGesPoint();
            tgp.actualTime = time;
            int x = 0;
            int y = 0;
            if (isTouch)
            {
                x = (int)((TouchDevice)inputDevice).GetTouchPoint(parentWindow).Position.X;
                y = (int)((TouchDevice)inputDevice).GetTouchPoint(parentWindow).Position.Y;
            }
            else
            {
                x = (int)((MouseDevice)inputDevice).GetPosition(parentWindow).X;
                y = (int)((MouseDevice)inputDevice).GetPosition(parentWindow).Y;
            }
            tgp.x = x;
            tgp.y = y;
            StylusPoint pt = new StylusPoint(x, y);
            long dt = time - startTime;
            if (dt != 0)
            {
                gestureData.Add(tgp);
                startTime = time;
                oldX = x;
                oldY = y;
            }
            return pt;
        }
    }
}
