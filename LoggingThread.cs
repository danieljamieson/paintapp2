﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;
using System.Threading;
using System.Windows.Input;
using System.IO;
using System.Windows.Shapes;
using System.Windows.Media;
using System.Windows.Ink;
using System.Windows;
using System.Windows.Controls;
using Microsoft.Surface.Presentation.Controls;

namespace PaintApp1
{
    public class LoggingThread
    {
        public Analyzer analyzer;

        public InputDevice inputDevice;
        public TouchGesture currentTouch;

        public Dictionary<TouchDevice, TouchGesture>    activeTouches;
        public Dictionary<TouchDevice, Stroke>          activeStrokes;

        public Dictionary<CWASensor, IMUGesture> cwas; // each CWA3 has it's own gesture

        private BackgroundWorker[] cwaLogger; // threading that logs data
        private Dictionary<int, int> threadSensorID = new Dictionary<int, int>();
        private BackgroundWorker touchLogger;

        private SurfaceWindow1 parentWindow;
        public List<TouchGesture> touchesList;

        private static int _cwaSleep = 10; //in msec
        private static int _touchSleep = 25;


        public static int TIMESCALE = 10000;
        private static int LOG_TIME_IN_SEC = 20;
        private static int DATA_LOG_SIZE = LOG_TIME_IN_SEC * 1000 / CWA_SLEEP;    //12.5 seconds 20 logs per second
        public static int DATA_MARGIN_SIZE = 30;//1000 / LOGGER_SLEEP; //(2 seconds worth of data)

        ComparisonWindow touch1 = new ComparisonWindow();
        ComparisonWindow touch2 = new ComparisonWindow();

        StreamWriter touchSW;
        StreamWriter imuSW;
        
        public static int CWA_SLEEP
        {
            get { return _cwaSleep; }
            set
            {
                _cwaSleep = value;
                DATA_LOG_SIZE = LOG_TIME_IN_SEC * 1000 / _cwaSleep;    //12.5 seconds 20 logs per second
            }
        }
        public static int TOUCH_SLEEP
        {
            get { return _touchSleep; }
            set
            {
                _touchSleep = value;
                DATA_LOG_SIZE = LOG_TIME_IN_SEC * 1000 / _touchSleep;    //12.5 seconds 20 logs per second
            }
        }

        public LoggingThread(SurfaceWindow1 pw, Analyzer _analyzer)
        {
            parentWindow = pw;
            touch1.Show();
            touch2.Show();
            //touchSW = new StreamWriter("daccs.txt");
            //imuSW = new StreamWriter("imuDaccs.txt");
            activeTouches = new Dictionary<TouchDevice, TouchGesture>();
            cwas = new Dictionary<CWASensor, IMUGesture>();

            analyzer = _analyzer;
        }

        public void StartLogging()
        {
            // Set up thread to poll sensors
            cwaLogger = new BackgroundWorker[SurfaceWindow1.SENSOR_COUNT];

            for (int i = 0; i < SurfaceWindow1.SENSOR_COUNT; i++)
            {
                cwaLogger[i] = new BackgroundWorker();
                cwaLogger[i].WorkerSupportsCancellation = true;
                cwaLogger[i].DoWork += new DoWorkEventHandler(cwaLogger_DoWork);
                cwaLogger[i].RunWorkerCompleted += new RunWorkerCompletedEventHandler(cwaLogger_Completed);
                cwaLogger[i].ProgressChanged += new ProgressChangedEventHandler(cwaLogger_ProgressChanged);
                cwaLogger[i].WorkerReportsProgress = true;
                cwaLogger[i].RunWorkerAsync();
                threadSensorID.Add(cwaLogger[i].GetHashCode(), i);
            }

            // Setup thread to poll touch X and Y
            touchLogger = new BackgroundWorker();
            touchLogger.WorkerSupportsCancellation = true;
            touchLogger.DoWork += new DoWorkEventHandler(touchLogger_DoWork);
            touchLogger.RunWorkerCompleted += new RunWorkerCompletedEventHandler(touchLogger_Completed);
            touchLogger.ProgressChanged += new ProgressChangedEventHandler(touchLogger_ProgressChanged);
            touchLogger.WorkerReportsProgress = true;
            touchLogger.RunWorkerAsync(0);
        }

        #region TouchUpDown funcs

        /// <summary>
        /// Called by the main window when a touch down event is recorded on the InkCanvas. A new touchGesture is
        /// created for this new touch event, and the initial point is added
        /// </summary>
        /// <param name="touchDevice">The touch event specific device which collects the touch points</param>
        public void touchDown(TouchDevice touchDevice, int strokeIndex)
        {
            TouchPoint pt = touchDevice.GetTouchPoint(parentWindow);
            //Console.WriteLine("x= " + pt.Position.X + ", y= " + pt.Position.Y);
            if (!analyzer.touches.ContainsKey(touchDevice))
            {
                //ONLINE
                if (analyzer.touches.Count == 0)
                    analyzer.touches.Add(touchDevice, AnalyzerState.newState(parentWindow, touch1, SurfaceWindow1.SENSOR_COUNT, touchSW, imuSW));
                else
                    analyzer.touches.Add(touchDevice, AnalyzerState.newState(parentWindow, touch2, SurfaceWindow1.SENSOR_COUNT, touchSW, imuSW));
            }
            if (!analyzer.strokeIndices.ContainsKey(touchDevice))
            {
                analyzer.strokeIndices.Add(touchDevice, strokeIndex);
            }
        }

        /// <summary>
        /// Called by the main window when a touch up event is recorded on the InkCanvas. Retieves the relevant
        /// touch gesture and calls the Analyzer to correlate the touch and IMU data
        /// </summary>
        /// <param name="touchDevice">The touch event specific device which collects the touch points</param>
        public void touchUp(TouchDevice touchDevice)
        {
            //SUMDIFF
            TouchDevice touchDownDevice = analyzer.deviceFromDeviceID(touchDevice.Id);
            if (touchDownDevice != null)
            {
                if (analyzer.touches[touchDownDevice].needsIdentity != 0)
                {
                    int identifiedSensorID = 0;
                    int i = analyzer.indexFromKey(touchDownDevice);
                    double minDiff = 0;
                    if (i >= 0)
                    {
                        int signalLength;
                        double[] corD = analyzer.correlateTouchAndIMUDiffResampleTouch(i, out signalLength);
                        minDiff = double.MaxValue;
                        for (int j = 0; j < corD.Length; j++)
                        {
                            if (corD[j] < minDiff)
                            {
                                minDiff = corD[j];
                                identifiedSensorID = j;
                            }
                        }
                    }
                    parentWindow.identifyStroke(touchDownDevice, identifiedSensorID);
                }

                analyzer.touches.Remove(touchDownDevice);
                analyzer.strokeIndices.Remove(touchDownDevice);
            }
        }

        #endregion

        #region TouchLogger stuff
        
        /// <summary>
        /// This function polls the touch screen
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void touchLogger_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            double time = e.ProgressPercentage / 1000.0;
            //Console.WriteLine(time);

            foreach (TouchDevice device in analyzer.touches.Keys)
            {
                TouchPoint initialPoint = device.GetTouchPoint(parentWindow);
                Point touchPointAsPoint = new Point(initialPoint.Position.X, initialPoint.Position.Y);

                Point screenSpacePoint = parentWindow.PointToScreen(touchPointAsPoint);
                TouchGesPoint tgp = TouchGesPoint.newPoint(screenSpacePoint.X, screenSpacePoint.Y);
                //Console.WriteLine("touch" + i + "- X= " + tp.Position.X + ", Y= " + tp.Position.Y);

                tgp.unifiedTime = time;
                tgp.actualTime = time;

                AnalyzerState state = analyzer.touches[device];
                if (state.needsIdentity == 1)
                {
                    state.addSample(tgp);
                }
            }
        }

        void touchLogger_DoWork(object sender, DoWorkEventArgs e)
        {
            int currentTime;
            int n=1;
            int sleepTime;
            while (!touchLogger.CancellationPending)
            {
                currentTime = (int)((System.DateTime.Now.Ticks / TIMESCALE) - SurfaceWindow1.startTime);
                touchLogger.ReportProgress(currentTime);
                sleepTime = n * TOUCH_SLEEP - currentTime;
                while (sleepTime < 0)
                {
                    n++;
                    sleepTime = n * TOUCH_SLEEP - currentTime;
                };
                Thread.Sleep(sleepTime);
                n++;
            }
            Console.WriteLine("touch logging thread cancelled");
        }

        void touchLogger_Completed(object sender, RunWorkerCompletedEventArgs e)
        {
            Console.WriteLine("touch logger completed");
        }

        #endregion

        #region CWA Logger

        /// <summary>
        /// This function is called on a seperate thread and polls the Sensor and 
        /// the touch screen for new samples
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void cwaLogger_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            double time = e.ProgressPercentage / 1000.0;
            int me = threadSensorID[(sender as BackgroundWorker).GetHashCode()];

            CWASensor cwa = cwas.Keys.ToArray()[me];
            List<IMUDataPoint> imuData = new List<IMUDataPoint>();

            // Try and retrieve as many samples as possible
            IMUDataPoint? dp = null;
            do
            {
                dp = cwa.PollPort();
                if (dp.HasValue)
                {
                    IMUDataPoint dp2 = dp.Value;

                    if (Math.Abs((cwa.DeviceTime - DateTime.Now).TotalMilliseconds) > 1000)
                    {
                        Console.WriteLine("Device time reset.");
                        cwa.ResetDeviceTime();
                    }

                    dp2.actualTime = ((cwa.DeviceTime.Ticks / TimeSpan.TicksPerMillisecond) - SurfaceWindow1.startTime) / 1000.0;
                    dp2.unifiedTime = time;
                    imuData.Add(dp2);

                    cwa.UpdateDeviceTime();
                }
            } while (dp.HasValue);

            // Now just need to add them to analyer state that hasn't been identified
            foreach (AnalyzerState state in analyzer.touches.Values)
            {
                if (state.needsIdentity == 1)
                {
                    state.addIMUSamples(me, imuData);
                }
            }
        }

        void cwaLogger_DoWork(object sender, DoWorkEventArgs e)
        {
            int me = threadSensorID[(sender as BackgroundWorker).GetHashCode()];
            int currentTime;
            int n = 1;
            int sleepTime;
            while (!cwaLogger[me].CancellationPending)
            {
                currentTime = (int)(System.DateTime.Now.Ticks / TIMESCALE - SurfaceWindow1.startTime);
                cwaLogger[me].ReportProgress(currentTime);
                sleepTime = n * CWA_SLEEP - currentTime;
                while (sleepTime < 0)
                {
                    n++;
                    sleepTime = n * CWA_SLEEP - currentTime;
                };
                Thread.Sleep(sleepTime);
                n++;
            }
        }

        void cwaLogger_Completed(object sender, RunWorkerCompletedEventArgs e)
        {
            int me = threadSensorID[(sender as BackgroundWorker).GetHashCode()];
            Console.WriteLine("cwa logger + " + me + " completed");
        }

        #endregion

        #region bwLogger stuff
        /*
        /// <summary>
        /// This function is called on a seperate thread and polls the Sensor and 
        /// the touch screen for new samples
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void bwLogger_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            //foreach (IMUDevice imuDev in IMUDevicesList)
            double time = e.ProgressPercentage / 1000.0;

            //if (cwas.Keys.ToArray()[0].initialised == true && cwas.Keys.ToArray()[1].initialised == true)
            //    parentWindow.sensorStatusLbl.Content = "sensors are connected";

            int[] successArray = new int[cwas.Count];
            List<IMUDataPoint>[] dpoints = new List<IMUDataPoint>[cwas.Count];

            // Update each CWA3 by taking their latest sample and adding it 
            // to the gesture which we will use for our correlation
            for (int i = 0; i < cwas.Count(); i++)
            {
                CWASensor cwa = cwas.Keys.ToArray()[i];
                List<IMUDataPoint> dpointsList = new List<IMUDataPoint>();
                IMUDataPoint? dp = null;

                do
                {
                    dp = cwa.PollPort();
                    if (dp.HasValue)
                    {
                        IMUDataPoint dp2 = dp.Value;
                        successArray[i] = 1;

                        if (Math.Abs((cwa.DeviceTime - DateTime.Now).TotalMilliseconds) > 500)
                        {
                            Console.WriteLine("Device time reset.");
                            cwa.ResetDeviceTime();
                        }

                        dp2.actualTime = (cwa.DeviceTime.Ticks / TimeSpan.TicksPerMillisecond) - SurfaceWindow1.startTime;
                        dp2.unifiedTime = time;
                        dpointsList.Add(dp2);

                        cwa.UpdateDeviceTime();
                    }
                    else
                    {
                        successArray[i] = 0;
                    }
                } while (dp.HasValue);
                dpoints[i] = dpointsList;
            }

            //if (true)
            //{
                //ONLINE
                for (int i = 0; i < analyzer.touches.Count; i++)
                {
                    TouchDevice device = analyzer.touches.Keys.ToArray()[i];
                    TouchPoint tp = analyzer.touches.Keys.ToArray()[i].GetTouchPoint(parentWindow);
                    Point pt = new Point(tp.Position.X, tp.Position.Y);
                    Point screenSpace = parentWindow.PointToScreen(pt);
                    TouchGesPoint tgp = TouchGesPoint.newPoint(screenSpace.X, screenSpace.Y);
                    Console.WriteLine("touch" + i + "- X= " + tp.Position.X + ", Y= " + tp.Position.Y);
                    tgp.actualTime = time;
                    tgp.unifiedTime = time;
                    AnalyzerState state = analyzer.touches.Values.ToArray()[i];
                    if (state.needsIdentity == 1)
                    {
                        state.addIMUSamples(dpoints);
                        state.addSample(tgp);
                    }
                }
            //}
        }

        void bwLogger_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {

        }

        void bwLogger_DoWork(object sender, DoWorkEventArgs e)
        {
            int currentTime;
            int n=1;
            int sleepTime;
            while (!bwLogger.CancellationPending)
            {
                currentTime = (int)(System.DateTime.Now.Ticks / TIMESCALE - SurfaceWindow1.startTime);
                bwLogger.ReportProgress(currentTime);
                sleepTime = n * LOGGER_SLEEP - currentTime;
                while (sleepTime < 0)
                {
                    n++;
                    sleepTime = n * LOGGER_SLEEP - currentTime;
                };
                Thread.Sleep(sleepTime);
                n++;
            }
        }*/

        #endregion

        #region Random Funcs

        public IMUGesture RetrieveIMUGestureByIndex(int i)
        {
            return cwas.Values.ToArray()[i];
        }

        public void ResetIMUGestureByIndex(int i)
        {
            cwas.Values.ToArray()[i] = new IMUGesture();
        }

        /// <summary>
        /// Checks to see whether polling both sensors was successful
        /// </summary>
        /// <param name="success"></param>
        /// <returns></returns>
        private bool successfulPolling(int[] success)
        {
            int total = 0;
            for (int i = 0; i < success.Length; i++)
            {
                total += success[i];
            }
            if (total == success.Length)
                return true;
            else
                return false;
        }

        private static StylusPointCollection touchGestureToStylusPointCollection(TouchGesture tg)
        {
            StylusPointCollection spc = new StylusPointCollection();
            foreach (TouchGesPoint tgp in tg.gestureData)
            {
                spc.Add(new StylusPoint(tgp.x, tgp.y));
            }
            return spc;
        }
        private static TouchGesture stylusPointCollectionToTouchGesture(StylusPointCollection spc)
        {
            TouchGesture touchGesture = new TouchGesture();
            foreach (StylusPoint sp in spc)
            {
                touchGesture.addTouchPoint(TouchGesPoint.newPoint(sp.X, sp.Y));
            }
            return touchGesture;
        }
        #endregion
    }
}
