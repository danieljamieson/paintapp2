﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Diagnostics;

namespace PaintApp1
{
    class Recogniser
    {
        public event EventHandler 
            FillTouch, RedoSpin, UndoSpin, EraseWave,
            BrushAction, PencilAction;

        private Stopwatch fillTimer, eraseTimer;

        const double HARD_TOUCH_XY_ACCEL_THRESHOLD = 3.4;
        const int HARD_TOUCH_Y_GYRO_THRESHOLD = 400;
        const int HARD_TOUCH_COUNT_THRESHOLD = 10;          // The positions of the above values must be within this threshold
        const int ERASE_POINT_THRESHOLD = 10;               // How many points should be above the threshold
        const int GESTURE_INTERVAL_MS = 1000;               // The minimum time interval between each individual gesture

        /// <summary>
        /// Recogniser constructor
        /// </summary>
        public Recogniser()
        {
            fillTimer = new Stopwatch();
            eraseTimer = new Stopwatch();
        }

        /// <summary>
        /// Update recogniser class with the incremented window
        /// </summary>
        /// <param name="window">Incremented window</param>
        public void UpdateWindow(SlidingWindow window)
        {
            // Use stopwatch to restrict each event to firing every 700ms
            if (!fillTimer.IsRunning)
            {
                // Fill/hard touch event
                bool start = false;
                int xyPos = window.ExceedsXYMagAccelThreshold(HARD_TOUCH_XY_ACCEL_THRESHOLD);
                int yPos = window.ExceedsYGyroThreshold(HARD_TOUCH_Y_GYRO_THRESHOLD);

                // Function returns -1 if unsuccessful
                if (xyPos != -1 && yPos != -1 && Math.Abs(xyPos - yPos) < HARD_TOUCH_COUNT_THRESHOLD)
                {
                    OnFillTouch();
                    start = true;
                }

                if (start)
                {
                    fillTimer.Start();
                }  
            }
            else if (fillTimer.ElapsedMilliseconds > GESTURE_INTERVAL_MS)
            {
                fillTimer.Reset();
            }

            if (!eraseTimer.IsRunning)
            {
                bool start = false;
                if (window.AccelYPeaks() > ERASE_POINT_THRESHOLD)
                {
                    OnEraseWave();
                    start = true;
                }

                if (start)
                {
                    eraseTimer.Start();
                }
            }
            else if (eraseTimer.ElapsedMilliseconds > GESTURE_INTERVAL_MS)
            {
                eraseTimer.Reset();
            }
        }

        /// <summary>
        /// Fires the fill touch event
        /// </summary>
        protected virtual void OnFillTouch()
        {
            if (FillTouch != null)
            {
                FillTouch(this, EventArgs.Empty);
            }
        }

        /// <summary>
        /// Fires the erase wave event
        /// </summary>
        protected virtual void OnEraseWave()
        {
            if (EraseWave != null)
            {
                EraseWave(this, EventArgs.Empty);
            }
        }

        /// <summary>
        /// Fires the undo spin event
        /// </summary>
        protected virtual void OnUndoSpin()
        {
            if (UndoSpin != null)
            {
                UndoSpin(this, EventArgs.Empty);
            }
        }
    }
}
