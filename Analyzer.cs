﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Media;
using System.ComponentModel;
using System.Threading;
using System.Windows.Input;
using System.Windows.Ink;
using System.IO;
using MathNet.Numerics.Interpolation.Algorithms;

namespace PaintApp1
{
    public class AnalyzerState 
    {
        public int needsIdentity;
        public int queueSize;

        ComparisonWindow cmp;

        public double[] finalDaccs;

        public double[][] finalimuD = new double[2][];
        
        public Queue<double> daccs;
        public Queue<double> times;
        public Queue<double> xs;
        public Queue<double> ys;
        public Queue<double> xvels;
        public Queue<double> yvels;
        SurfaceWindow1 parentWindow;

        public Queue<double>[] imuTimes;
        public Queue<double>[] imuLinDQueue;
        public Queue<double>[] imuLinXQueue;
        public Queue<double>[] imuLinYQueue;

        StreamWriter imuDaccs;
        StreamWriter touchDaccs;

        double lastXAccel = 0, lastYAccel = 0;

        public static AnalyzerState newState(SurfaceWindow1 parent, ComparisonWindow compare, int sensorCount, StreamWriter touchSW, StreamWriter
             imuSW)
        {
            AnalyzerState state = new AnalyzerState();
            state.cmp = compare;
            state.needsIdentity = 1;
            state.queueSize = Analyzer.FILTER + 2;
            state.parentWindow = parent;
            state.daccs = new Queue<double>();
            state.times = new Queue<double>();
            state.xs = new Queue<double>();
            state.ys = new Queue<double>();
            state.xvels = new Queue<double>();
            state.yvels = new Queue<double>();
            state.imuTimes = new Queue<double>[sensorCount];
            state.imuLinDQueue = new Queue<double>[sensorCount];
            state.imuLinXQueue = new Queue<double>[sensorCount];
            state.imuLinYQueue = new Queue<double>[sensorCount];
            Analyzer.initialiseQueueArray(state.imuTimes);
            Analyzer.initialiseQueueArray(state.imuLinDQueue);
            Analyzer.initialiseQueueArray(state.imuLinXQueue);
            Analyzer.initialiseQueueArray(state.imuLinYQueue);
            state.imuDaccs = imuSW;
            state.touchDaccs = touchSW;
            return state;
        }

        public static AnalyzerState newState(SurfaceWindow1 parent, ComparisonWindow compare, int sensorCount) {
            AnalyzerState state = new AnalyzerState();
            state.cmp = compare;
            state.needsIdentity = 1;
            state.queueSize = Analyzer.FILTER + 2;
            state.parentWindow = parent;
            state.daccs = new Queue<double>();
            state.times = new Queue<double>();
            state.xs = new Queue<double>();
            state.ys = new Queue<double>();
            state.xvels = new Queue<double>();
            state.yvels = new Queue<double>();
            state.imuLinDQueue = new Queue<double>[sensorCount];
            state.imuLinXQueue = new Queue<double>[sensorCount];
            state.imuLinYQueue = new Queue<double>[sensorCount];
            Analyzer.initialiseQueueArray(state.imuLinDQueue);
            Analyzer.initialiseQueueArray(state.imuLinXQueue);
            Analyzer.initialiseQueueArray(state.imuLinYQueue);
            state.imuDaccs = new StreamWriter("imudaccs.txt");
            state.touchDaccs = new StreamWriter("daccs.txt");
            return state;
        }

        public void shiftQueues() {
            //daccs = Analyzer.shiftQueue(daccs, Analyzer.QUEUE_SIZE);
            times = Analyzer.shiftQueue(times, Analyzer.QUEUE_SIZE);
            xs = Analyzer.shiftQueue(xs, Analyzer.QUEUE_SIZE);
            ys = Analyzer.shiftQueue(ys, Analyzer.QUEUE_SIZE);
        }

        public void addIMUSamples(List<IMUDataPoint>[] imuDataPoints)
        {
            // Loop through each sensor and get samples linear accel x and y
            for (int i = 0; i < SurfaceWindow1.SENSOR_COUNT; i++)
            {
                IMUDataPoint[] ptList = imuDataPoints[i].ToArray();
                for (int j = 0; j < ptList.Length; j++)
                {
                    imuLinXQueue[i].Enqueue(ptList[j].linearAcc[0]);
                    imuLinXQueue[i] = Analyzer.shiftQueue(imuLinXQueue[i], Analyzer.smallQueueSize);
                    imuLinYQueue[i].Enqueue(ptList[j].linearAcc[2]);
                    imuLinYQueue[i] = Analyzer.shiftQueue(imuLinYQueue[i], Analyzer.smallQueueSize);

                    if (imuLinXQueue[i].Count > 0 && imuLinYQueue[i].Count > 0)
                    {
                        double x = imuLinXQueue[i].ToArray()[imuLinXQueue[i].Count - 1];
                        double y = imuLinYQueue[i].ToArray()[imuLinYQueue[i].Count - 1];
                        double newImuLinDVal = Math.Sqrt(Math.Pow(x, 2) + Math.Pow(y, 2));
                        imuLinDQueue[i].Enqueue(newImuLinDVal);
                        imuLinDQueue[i] = Analyzer.shiftQueue(imuLinDQueue[i], Analyzer.QUEUE_SIZE);
                        imuLinDQueue[i].ToArray()[0] = 0;
                        if (imuLinDQueue[i].Count > 1) imuLinDQueue[i].ToArray()[1] = 0;

                        imuDaccs.WriteLine(x + " " + y + " " + newImuLinDVal);
                    }
                }
            }
        }

        public void addIMUSamples(int i, List<IMUDataPoint> imuDataPoints)
        {
            IMUDataPoint[] ptList = imuDataPoints.ToArray();
            for (int j = 0; j < ptList.Length; j++)
            {
                // Add to the x and y queue
                imuTimes[i].Enqueue(ptList[j].actualTime);
                imuTimes[i] = Analyzer.shiftQueue(imuTimes[i], Analyzer.QUEUE_SIZE);
                imuLinXQueue[i].Enqueue(ptList[j].linearAcc[0]);
                imuLinXQueue[i] = Analyzer.shiftQueue(imuLinXQueue[i], Analyzer.QUEUE_SIZE);
                imuLinYQueue[i].Enqueue(ptList[j].linearAcc[2]);
                imuLinYQueue[i] = Analyzer.shiftQueue(imuLinYQueue[i], Analyzer.QUEUE_SIZE);

                // If we have more than 0 values in the queue we can start calculating magnitude
                if (imuLinXQueue[i].Count > 0 && imuLinYQueue[i].Count > 0)
                {
                    double x = imuLinXQueue[i].ToArray()[imuLinXQueue[i].Count - 1];
                    double y = imuLinYQueue[i].ToArray()[imuLinYQueue[i].Count - 1];
                    double newImuLinDVal = Math.Sqrt(Math.Pow(x, 2) + Math.Pow(y, 2));
                    imuLinDQueue[i].Enqueue(newImuLinDVal);
                    imuLinDQueue[i] = Analyzer.shiftQueue(imuLinDQueue[i], Analyzer.QUEUE_SIZE);

                    imuLinDQueue[i].ToArray()[0] = 0; // We might want to lower the playing field, and we might not
                    if (imuLinDQueue[i].Count > 1) imuLinDQueue[i].ToArray()[1] = 0;
                }
            }
        }

        public void addSample(TouchGesPoint pt) {
            times.Enqueue(pt.actualTime);
            double tx = pt.x;
            double ty = pt.y;
            double actualX = tx / parentWindow.dpiX; actualX *= 0.0254;
            double actualY = ty / parentWindow.dpiY; actualY *= 0.0254;
            xs.Enqueue(actualX);
            ys.Enqueue(actualY);

            shiftQueues();
            /*
            // If we have at least two samples then we can calculate a new velocity
            double dt = 0;
            if (xs.Count > 1 && ys.Count > 1)
            {
                int n = xs.Count - 1;
                dt = (times.ToArray()[n] - times.ToArray()[n - 1]);

                double newXv = (xs.ToArray()[n] - xs.ToArray()[n - 1]) / dt;
                double newYv = (ys.ToArray()[n] - ys.ToArray()[n - 1]) / dt;

                if (dt == 0)
                {
                    if (xvels.Count > 0 && yvels.Count > 0)
                    {
                        newXv = xvels.ToArray()[xvels.Count - 1];
                        newYv = yvels.ToArray()[yvels.Count - 1];
                    }
                    else
                    {
                        newXv = 0;
                        newYv = 0;
                    }
                }
                xvels.Enqueue(newXv); xvels = Analyzer.shiftQueue(xvels, 2);
                yvels.Enqueue(newYv); yvels = Analyzer.shiftQueue(yvels, 2);
            }
            else
            {
                xvels.Enqueue(0.0); xvels = Analyzer.shiftQueue(xvels, 2);
                yvels.Enqueue(0.0); yvels = Analyzer.shiftQueue(yvels, 2);
            }

            // If we have at least two velocities then we can calculate a new acceleration
            if (xvels.Count > 1 && yvels.Count > 1)
            {
                double newXa = (xvels.ToArray()[1] - xvels.ToArray()[0]) / dt;
                double newYa = (yvels.ToArray()[1] - yvels.ToArray()[0]) / dt;

                if (dt == 0)
                {
                    newXa = lastXAccel;
                    newYa = lastYAccel;
                }

                double newDaccs = Math.Sqrt(Math.Pow(newXa, 2) + Math.Pow(newYa, 2));

                //touchDaccs.WriteLine(newXa + " " + newYa + " " + newDaccs);

                daccs.Enqueue(newDaccs);
                daccs = Analyzer.shiftQueue(daccs, Analyzer.QUEUE_SIZE);
                if (daccs.Count > 1) daccs.ToArray()[1] = 0;

                lastXAccel = newXa; lastYAccel = newYa;
            }
            else
            {
                daccs.Enqueue(0.0);
                daccs = Analyzer.shiftQueue(daccs, Analyzer.QUEUE_SIZE);
                lastXAccel = 0; lastYAccel = 0;
            }*/
        }

        public void Identified() {
            this.needsIdentity = 0;
        }

        public void drawGraphs(double[] dacc, double[] sensor1, double[] sensor2)
        {
            if (dacc.Length > 0 && sensor1.Length > 0 && sensor2.Length > 0)
            {
                cmp.drawGraphs(dacc, sensor1, sensor2);
                /*cmp.drawGraph(dacc, 0, true);
                cmp.drawGraph(sensor1, 1, false);
                cmp.drawGraph(sensor2, 2, false);*/
            }
        }
        public void drawGraphs(double[] dacc1, double[] dacc2, double[] sensor1, double[] sensor2)
        {
            if (dacc1.Length > 0 && dacc2.Length > 0 && sensor1.Length > 0 && sensor2.Length > 0)
            {
                cmp.drawGraphs(dacc1, dacc2, sensor1, sensor2);
                /*cmp.drawGraph(dacc, 0, true);
                cmp.drawGraph(sensor1, 1, false);
                cmp.drawGraph(sensor2, 2, false);*/
            }
        }
    }

    public class Analyzer
    {
        BackgroundWorker corrBW; // background worker for calculating correlation at regular intervals

        public bool initalisedOnline = false;
        public int SENSOR_COUNT;
        
        public const int QUEUE_SIZE = 2000;
        public static int FILTER; // number of samples we need to refresh each iteration

        public static int smallQueueSize = 7;

        public Dictionary<TouchDevice, AnalyzerState> touches = new Dictionary<TouchDevice,AnalyzerState>();
        public Dictionary<TouchDevice, int> strokeIndices;

        // For making identification
        SurfaceWindow1 parentWindow;
        // For drawing graphs
        private ComparisonWindow cmp;
        private int imuGraphIndex = 1;
        private double maxTouchD = 0;
        private double maxIMUD = 0;

        private static int _analyzerSleep = 500; //in msec
        
        public static int ANALYZER_SLEEP
        {
            get { return _analyzerSleep; }
            set
            {
                _analyzerSleep = value;
            }
        }

        public Analyzer(SurfaceWindow1 parent)
        {
            parentWindow = parent;
        }

        public void initialiseOnline(int sensorCount, int filterVal)
        {
            SENSOR_COUNT = sensorCount;
            FILTER       = filterVal;
            touches      = new Dictionary<TouchDevice, AnalyzerState>();
            strokeIndices      = new Dictionary<TouchDevice, int>();
            initalisedOnline = true;

            corrBW = new BackgroundWorker();
            corrBW.WorkerSupportsCancellation = true;
            corrBW.DoWork += new DoWorkEventHandler(corrBW_DoWork);
            corrBW.ProgressChanged += new ProgressChangedEventHandler(corrBW_OnlineUpdate);
            corrBW.RunWorkerCompleted += new RunWorkerCompletedEventHandler(corrBW_RunWorkerCompleted);
            corrBW.WorkerReportsProgress = true;
            //corrBW.RunWorkerAsync();
        }

        void corrBW_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {

        }

        void corrBW_DoWork(object sender, DoWorkEventArgs e)
        {
            int currentTime;
            int n = 1;
            int sleepTime;
            while (!corrBW.CancellationPending)
            {
                currentTime = (int)(System.DateTime.Now.Ticks / LoggingThread.TIMESCALE - SurfaceWindow1.startTime);
                
                // We only want to calculate the correlation if there's touches left to identify
                if (touchesLeftToIdentify() > 0)
                {
                    corrBW.ReportProgress(currentTime);

                    sleepTime = n * ANALYZER_SLEEP - currentTime;
                    while (sleepTime < 0)
                    {
                        n++;
                        sleepTime = n * ANALYZER_SLEEP - currentTime;
                    };
                    Thread.Sleep(sleepTime);
                    n++;
                }
            }
        }

        public int indexFromKey(TouchDevice key)
        {
            for (int i = 0; i < touches.Count; i++)
            {
                if (touches.Keys.ToArray()[i] == key)
                {
                    return i;
                }
            }
            return -1;
        }

        public TouchDevice deviceFromDeviceID(int deviceID)
        {
            for (int i = 0; i < touches.Count; i++)
            {
                if (touches.Keys.ToArray()[i].Id == deviceID)
                    return touches.Keys.ToArray()[i];
            }
            return null;
        }

        public int touchesLeftToIdentify()
        {
            int count = 0;
            for (int i = 0; i < touches.Count; i++)
            {
                try
                {
                    count += touches.Values.ToArray()[i].needsIdentity;
                }
                catch (IndexOutOfRangeException e)
                {
                    Console.WriteLine("IndexOutOfRangeException: " + e.StackTrace);
                }
                catch (NullReferenceException e)
                {
                    Console.WriteLine("NullReferenceException: " + e.StackTrace);
                }
                catch (ArgumentException e)
                {
                    Console.WriteLine("ArgumentException: " + e.StackTrace);
                }
            }
            return count;
        }

        void corrBW_OnlineUpdate(object sender, ProgressChangedEventArgs e)
        {
            // Create an array to store the correlation values, and initialise it
            double[][] corD = new double[touches.Count][];
            for (int i = 0; i < corD.Length; i++) {
                corD[i] = new double[SENSOR_COUNT];
            }

            int[] signalLength = new int[touches.Count];
            for (int i = 0; i < touches.Count; i++)
            {
                //if (touches.Values.ToArray()[i].daccs.Count < 20)
                //    continue;
                corD[i] = correlateTouchAndIMUDiffResampleTouch(i, out signalLength[i]);
            }
            
            //parentWindow.makeOnlineIdentification(touches.Keys.ToArray(), corD, signalLength);
            parentWindow.makeMagnitudeDiffBasedOnlineIdentification(touches.Keys.ToArray(), corD, signalLength);
        }

        public double[] correlateTouchAndIMUDiff(int i, out int signalLength)
        {
            double[] corD = new double[SENSOR_COUNT];
            AnalyzerState state = touches.Values.ToArray()[i];
            //if (state.daccs.Count < 100) return new double[] { -1, -1 };

            double[][] imuLinDVals = new double[SENSOR_COUNT][];
            double[] times = state.times.ToArray();
            double[] daccs = state.daccs.ToArray();

            if (daccs.Length > 0) daccs[0] = 0;
            if (daccs.Length > 1) daccs[1] = 0;
            if (daccs.Length > 0) daccs[daccs.Length - 1] = 0;

            //daccs = filterData(daccs, FILTER);

            signalLength = daccs.Length;

            // calculate the correlation for each sensor
            for (int j = 0; j < SENSOR_COUNT; j++)
            {
                double[] imuTimes = state.imuTimes[j].ToArray();
                if (imuTimes.Length < 5)
                    continue;
                double[] imuLinXVals = resampleIMU(state.imuLinXQueue[j].ToArray(), imuTimes, times);
                double[] imuLinYVals = resampleIMU(state.imuLinYQueue[j].ToArray(), imuTimes, times);
                //double[] imuLinXVals = state.imuLinXQueue[j].ToArray();
                //double[] imuLinYVals = state.imuLinYQueue[j].ToArray();

                imuLinDVals[j] = new double[imuLinXVals.Length];
                for (int k = 0; k < imuLinXVals.Length; k++)
                {
                    imuLinDVals[j][k] = Math.Sqrt(Math.Pow(imuLinXVals[k], 2) + Math.Pow(imuLinYVals[k], 2));
                }

                if (imuLinDVals[j].Length > 0) imuLinDVals[j][0] = 0;
                if (imuLinDVals[j].Length > 1) imuLinDVals[j][1] = 0;
                if (imuLinDVals[j].Length > 0) imuLinDVals[j][imuLinDVals[j].Length - 1] = 0;
                
                if (daccs.Length == imuLinDVals[j].Length)
                {
                    daccs = filterData(daccs, 5);
                    //daccs = filterData(daccs, 3);
                    imuLinDVals[j] = filterData(imuLinDVals[j], 5);
                    imuLinDVals[j] = filterData(imuLinDVals[j], 3);
                    double sigma = 0;
                    corD[j] = magnitudeDifference(daccs, imuLinDVals[j], out sigma);
                    Console.WriteLine(i + ", " + j + ": diff- " + corD[j] + " sigma- " + sigma);
                }
            }

            if (imuLinDVals[0] != null && imuLinDVals[1] != null)
            {
                state.drawGraphs(daccs, imuLinDVals[0], imuLinDVals[1]);
                state.finalDaccs = daccs;
                state.finalimuD[0] = imuLinDVals[0];
                state.finalimuD[1] = imuLinDVals[1];
            } 
            return corD;
        }

        public double[] correlateTouchAndIMUDiffResampleTouch(int i, out int signalLength)
        {
            double[] corD = new double[SENSOR_COUNT];
            AnalyzerState state = touches.Values.ToArray()[i];
            signalLength = state.xs.Count;
            if (state.xs.Count < 5) return corD;

            double[][] imuLinDVals = new double[SENSOR_COUNT][];
            double[] times = state.times.ToArray();
            double[][] daccs = new double[SENSOR_COUNT][];

            // calculate the correlation for each sensor
            for (int j = 0; j < SENSOR_COUNT; j++)
            {
                double range = 0;
                double[] imuTimes = state.imuTimes[j].ToArray();
                double[] xs = resampleIMU(state.xs.ToArray(), times, imuTimes);
                double[] ys = resampleIMU(state.ys.ToArray(), times, imuTimes);
                double[] xvels = getDerivatives(xs, imuTimes, out range);
                double[] yvels = getDerivatives(ys, imuTimes, out range);
                double[] xaccs = getDerivatives(xvels, imuTimes, out range);
                double[] yaccs = getDerivatives(yvels, imuTimes, out range);

                daccs[j] = new double[xaccs.Length];
                for (int k = 0; k < daccs[j].Length; k++)
                {
                    daccs[j][k] = Math.Sqrt(Math.Pow(xaccs[k], 2) + Math.Pow(yaccs[k], 2));
                }

                if (daccs[j].Length > 0) daccs[j][0] = 0;
                if (daccs[j].Length > 1) daccs[j][1] = 0;
                if (daccs[j].Length > 0) daccs[j][daccs[j].Length - 1] = 0;

                imuLinDVals[j] = state.imuLinDQueue[j].ToArray();

                if (imuLinDVals[j].Length > 0) imuLinDVals[j][0] = 0;
                if (imuLinDVals[j].Length > 1) imuLinDVals[j][1] = 0;
                if (imuLinDVals[j].Length > 0) imuLinDVals[j][imuLinDVals[j].Length - 1] = 0;

                if (daccs[j].Length == imuLinDVals[j].Length)
                {
                    daccs[j] = filterData(daccs[j], 5);
                    imuLinDVals[j] = filterData(imuLinDVals[j], 3);
                    imuLinDVals[j] = addDelay(imuLinDVals[j], -4);
                    double sigma = 0;
                    corD[j] = magnitudeDifference(daccs[j], imuLinDVals[j], out sigma);
                    Console.WriteLine(i + ", " + j + ": diff- " + corD[j] + " sigma- " + sigma);
                }
            }

            //if (imuLinDVals[0] != null && imuLinDVals[1] != null)
            //{
                state.drawGraphs(daccs[0], daccs[1], imuLinDVals[0], imuLinDVals[1]);
                state.finalDaccs = daccs[0];
                state.finalimuD[0] = imuLinDVals[0];
                state.finalimuD[1] = imuLinDVals[1];
            /*}
            else
            {
                Console.WriteLine("could not draw graphs because of null array");
            }*/
            signalLength = daccs.Length;
            return corD;
        }

        private double[] resampleIMU(double[] values, double[] times, double[] resampledTimes)
        {
            double[] resampledValues = new double[resampledTimes.Length];
            // check that they're in ascending order
            double min = double.MaxValue;
            int minIndex = 0;
            int minCounter = 0;
            for (int i = 0; i < times.Length; i++)
            {
                if (times[i] <= min)
                {
                    min = times[i];
                    minIndex = i;
                    minCounter++;
                }
            }
            if (minCounter > 1)
            {
                List<double> vals = values.ToList();
                List<double> time = times.ToList();
                vals.RemoveRange(0, minIndex - 1);
                time.RemoveRange(0, minIndex - 1);
                values = vals.ToArray();
                times = time.ToArray();
            }
            
            CubicSplineInterpolation csi = new CubicSplineInterpolation(times, values);

            for (int i = 0; i < resampledTimes.Length; i++)
            {
                resampledValues[i] = csi.Interpolate(resampledTimes[i]);
            }
            return resampledValues;
        }

        public double[] correlateTouchAndIMU(int i, out int signalLength)
        {
            double[] corD = new double[SENSOR_COUNT];
            AnalyzerState state = touches.Values.ToArray()[i];
            //if (state.daccs.Count < 100) return new double[] { -1, -1 };
            
            double[][] imuLinDVals = new double[SENSOR_COUNT][];
            double[] daccs = state.daccs.ToArray();//filterData(state.daccs.ToArray(), 3);

            daccs = filterData(daccs, FILTER);
            daccs = filterData(daccs, 3);

            signalLength = daccs.Length;

            // calculate the correlation for each sensor
            for (int j = 0; j < SENSOR_COUNT; j++)
            {
                double[] imuLinXVals = state.imuLinXQueue[j].ToArray();
                double[] imuLinYVals = state.imuLinYQueue[j].ToArray();

                // shift the imu queue so that it matches lengths with the touch queue
                Queue<double> tempDqueue = shiftQueue(state.imuLinDQueue[j], state.daccs.Count);
                imuLinDVals[j] = filterData(tempDqueue.ToArray(), FILTER);
                imuLinDVals[j] = filterData(imuLinDVals[j], 3);

                int arraySize = daccs.Length / 10;
                double[] touchPause = new double[arraySize];
                for (int k = 0; k < touchPause.Length; k++) {
                    touchPause[k] = 0;
                }
                Random random = new Random();
                double[] imuPause = new double[arraySize];
                for (int k = 0; k < imuPause.Length; k++) {
                    imuPause[k] = 0.01 + random.NextDouble() * 0.005;
                }

                /*List<double> tempDaccs = new List<double>();
                tempDaccs.AddRange(touchPause);
                tempDaccs.AddRange(daccs);
                tempDaccs.AddRange(touchPause);
                List<double> tempIMU = new List<double>();
                tempIMU.AddRange(imuPause);
                tempIMU.AddRange(imuLinDVals[j]);
                tempIMU.AddRange(imuPause);*/

                if (state.daccs.Count == state.imuLinDQueue[j].Count)
                {
                    //corD[j] = getCorrelation(tempDaccs.ToArray(), tempIMU.ToArray());
                    corD[j] = getCorrelation(daccs, imuLinDVals[j]);
                }
            }

            state.drawGraphs(daccs, imuLinDVals[0], imuLinDVals[1]);

            //if (state.daccs.Count < 60)
             //  return new double[] { -99, -99 };
            for (int l = 0; l < corD.Length; l++)
            {
                if (corD[l] > 0.61)
                {
                    Console.WriteLine("threshold hit. count is : " + state.daccs.Count);
                }
            }
            return corD;
        }

        public static Queue<double> shiftQueue(Queue<double> queue, int queueSize)
        {
            if (queue.Count <= queueSize)
                return queue;
            int overfill = queue.Count - queueSize;
            for (int i = 0; i < overfill; i++)
            {
                queue.Dequeue();
            }
            return queue;
        }

        public static void initialiseQueueArray(Queue<double>[] queueArray)
        {
            for (int i = 0; i < queueArray.Length; i++)
            {
                queueArray[i] = new Queue<double>();
            }
        }

        public int guessOptimiumDelayFix(TouchGesture tg, IMUGesture allIMUPoints, int extraReadings, int filterVal, bool resampleData)
        {
            int bestDelay = 0;
            double bestCorD = -2;
            double corX,corY,corD;
            for (int delay = -5; delay <= 5; delay++)
            {
                getCorrelationFromRawGestureData(tg, allIMUPoints, out corX, out corY, out corD, delay, filterVal, resampleData, false, false);
                if (corD > bestCorD)
                {
                    bestCorD = corD;
                    bestDelay = delay;
                }
            }
            return bestDelay;
        }

        public double[] addDelay(double[] y, int d)
        {
            double[] newY = new double[y.Length];

            for (int i = 0; i < y.Length; i++)
            {
                int j = i + d;
                if (j > 0 && j < y.Length)
                {
                    newY[j] = y[i];
                }
            }
            return newY;
        }

        /// <summary>
        /// Calculates the correlation between touchGesture and IMUGesture data for distance, x, y, and angle using the transofmationAngle and Delay provided.
        /// </summary>
        /// <param name="tg">The TouchGesture: from a touch screen, or even mouse data</param>
        /// <param name="imug">the IMUGesture: from the imu data</param>
        /// <param name="corX">Output parameter to return the correlation with respect to acceleration along the X asis</param>
        /// <param name="corY">Output parameter to return the correlation with respect to acceleration along the Y asis</param>
        /// <param name="corD">Output parameter to return the correlation with respect to acceleration distance</param>
        /// <param name="corAngle">Output parameter to return the correlation with respect to change an Angle</param>
        /// <param name="rotTrans">Rotation transform to be applied to the tg data to match its orientation to imug</param>
        /// <param name="delay">a time correction to be applied to the imug data. Basically, the imug includes 30 readings before and 30 readings after the 
        /// duration of the touchEvent at 50msec intervals. For a matching signal, we should ignore the first and last 30 reading. However, due to timing
        /// issues, sometimes we get better results if we go back or forward 1 or 2 samples. delay is basically the number of samples to shift and in 
        /// which direction</param>
        /// <param name="filterVal">The value of the moving average filter to use. It can be 0 (no filter), 3, or 5</param>
        /// <param name="bDraw">A UI flag to indicate whether to draw graphs while duing the calculations or not</param>
        /// <returns>void</returns>
        public void getCorrelationFromRawGestureData(TouchGesture tg, IMUGesture allIMUPoints, out double corX, out double corY, out double corD, 
            int delay, int filterVal, bool resampleData, bool drawTouch, bool drawIMU)
        {
            if (tg.gestureData.Count <= 2)
            {
                corX = corY = corD = 0;
                return;
            }

            double[] xs;
            double[] ys;
            double[] xvels;
            double[] yvels;
            double[] xaccs;
            double[] yaccs;
            double[] actualTimes;
            double[] resampledTimes;
            int extraEntries = 5;
            double[] daccs = new double[tg.gestureData.Count];
            double[] imuLinXVals = new double[tg.gestureData.Count];
            double[] imuLinYVals = new double[tg.gestureData.Count];
            double[] imuLinDVals = new double[tg.gestureData.Count + extraEntries];
            double[] actualIMUTimes = new double[tg.gestureData.Count];

            xs = new double[tg.gestureData.Count()];
            ys = new double[tg.gestureData.Count()];
            actualTimes = new double[xs.Length];

            int i, j;
            maxTouchD = 0;
            for (i = 0, j = allIMUPoints.indexOfGestureInList + delay; i < tg.gestureData.Count; i++, j++)
            {
                xs[i] = tg.gestureData[i].x;
                ys[i] = tg.gestureData[i].y;
                actualTimes[i] = tg.gestureData[i].actualTime;
                if (j > 0 && j < allIMUPoints.gestureData.Count)// imug.gestureData.Count)
                {
                    actualIMUTimes[i] = allIMUPoints.gestureData[j].actualTime; //imug.gestureData[j].actualTime;
                    imuLinXVals[i] = allIMUPoints.gestureData[j].linearAcc[0];
                    imuLinYVals[i] = allIMUPoints.gestureData[j].linearAcc[2];
                }
            }

            double samplingInterval;
            double[] dummy = new double[xs.Length];
            double vxRange, vyRange;
            double axRange, ayRange;
            if (resampleData)
            {
                xs = resample(xs, actualTimes, out resampledTimes, out samplingInterval);
                ys = resample(ys, actualTimes, out resampledTimes, out samplingInterval);
                xs = filterData(xs, filterVal);
                ys = filterData(ys, filterVal);
                xvels = getDerivatives(xs, resampledTimes, out vxRange);
                yvels = getDerivatives(ys, resampledTimes, out vyRange);
                //xvels = filterData(xvels, filterVal);
                //yvels = filterData(yvels, filterVal);
                xaccs = getDerivatives(xvels, resampledTimes, out axRange);
                yaccs = getDerivatives(yvels, resampledTimes, out ayRange);

                imuLinXVals = resample(imuLinXVals, actualIMUTimes, samplingInterval);
                imuLinYVals = resample(imuLinYVals, actualIMUTimes, samplingInterval);
            }
            else
            {
                xs = filterData(xs, filterVal);
                ys = filterData(ys, filterVal);
                xvels = getDerivatives(xs, actualTimes, out vxRange);
                yvels = getDerivatives(ys, actualTimes, out vyRange);
                xaccs = getDerivatives(xvels, actualTimes, out axRange);
                yaccs = getDerivatives(yvels, actualTimes, out ayRange);
            }

            xaccs[1] = 0;   //it is incorrect because v[0] is unknown
            yaccs[1] = 0;   //it is incorrect because v[0] is unknown

            imuLinXVals[0] = imuLinXVals[1] = 0; //to give it the same initial conditions as the touch data
            imuLinYVals[0] = imuLinYVals[1] = 0; //to give it the same initial conditions as the touch data

            imuLinXVals[xaccs.Length - 1] = 0;
            imuLinYVals[xaccs.Length - 1] = 0;

            //to level the playing field
            xaccs[xaccs.Length - 1] = 0;
            yaccs[xaccs.Length - 1] = 0;

            if (filterVal != 0)
            {
                xaccs = filterData(xaccs, filterVal);
                yaccs = filterData(yaccs, filterVal);
                imuLinXVals = filterData(imuLinXVals, filterVal);
                imuLinYVals = filterData(imuLinYVals, filterVal);
            }

            for (i = 0; i < xaccs.Length; i++)
            {
                daccs[i] = Math.Sqrt(Math.Pow(xaccs[i], 2) + Math.Pow(yaccs[i], 2));
                if (daccs[i] > maxTouchD)
                    maxTouchD = daccs[i];
                imuLinDVals[i] = Math.Sqrt(Math.Pow(imuLinXVals[i], 2) + Math.Pow(imuLinYVals[i], 2));
                if (imuLinDVals[i] > maxIMUD)
                    maxIMUD = imuLinDVals[i];
            }

            //maxIMUD = 0.6;
            //{//add extras
            //    daccs[0] = daccs[1] = 0;
            //    imuLinDVals[0] = imuLinDVals[1] = 0;
            //    for (i = xaccs.Length - 1, j = 0; j < extraEntries; i++, j++)
            //    {
            //        daccs[i] = maxTouchD;
            //        imuLinDVals[i] = maxIMUD;
            //    }
            //}

            //corX = getCorrelation(xaccs, imuLinXVals);
            //corY = getCorrelation(yaccs, imuLinYVals);
            corX = 0;
            corY = 0;

            if (drawTouch)
            {
                cmp.drawGraph(daccs, 0, true);
            }
            if (drawIMU)
            {
                cmp.drawGraph(imuLinDVals, imuGraphIndex, false);
                imuGraphIndex++;
                if (imuGraphIndex > 2) imuGraphIndex = 1;
            }
            double sigma = 0;
            corD = magnitudeDifference(daccs, imuLinDVals, out sigma);
        }

        double[] getDerivatives(double[] ds, double[] times, out double valueRange)
        {
            double min=double.MaxValue,max = double.MinValue;
            double[] vels = new double[ds.Length];
            for (int i = 0; i < ds.Length; i++)
            {
                if (i > 0)
                {
                    if (times[i] == times[i - 1])
                        vels[i] = vels[i - 1];
                    else
                        vels[i] = (ds[i] - ds[i - 1]) / (times[i] - times[i - 1]);
                }
                else
                    vels[i] = 0;
                if (vels[i] > max)
                    max = vels[i];
                if (vels[i] < min)
                    min = vels[i];
            }
            valueRange = max - min;
            return vels;
        }

        //calculates a suitable interval and filles in the resampeTimes
        public static double[] resample(double[] vals, double[] actualTimes, out double[] resampledTimes, out double interval)
        {
            double[] resampledVals = new double[vals.Length];
            resampledTimes = new double[vals.Length];
            double initialTime = actualTimes[0];
            interval = (actualTimes[vals.Length - 1] - initialTime) / (vals.Length - 1);
            double currentTime = initialTime;
            for (int i = 0; i < vals.Length; i++)
            {
                resampledTimes[i] = currentTime;
                if (i > 0 && i < (vals.Length - 1))
                    resampledVals[i] = getInterpolatedValue(i, vals, actualTimes, currentTime);
                else
                    resampledVals[i] = vals[i];
                currentTime += interval;
            }
            return resampledVals;
        }

        //takes in the resample times as input
        public static double[] resample(double[] vals, double[] actualTimes, double samplingInterval)
        {
            double[] resampledVals = new double[vals.Length];
            double currentTime = actualTimes[0];
            for (int i = 0; i < vals.Length; i++)
            {
                if (i > 0 && i < (vals.Length - 1))
                    resampledVals[i] = getInterpolatedValue(i, vals, actualTimes, currentTime);
                else
                    resampledVals[i] = vals[i];
                currentTime += samplingInterval;
            }
            return resampledVals;
        }


        public static double getInterpolatedValue(int i, double[] vals, double[] times, double timeStamp)
        {
            double iVal;
            if (times[i] == times[i - 1])
            {
                if(i<(vals.Length-1))
                    return (vals[i+1]+vals[i-1])/2.0;
                else
                    return vals[i];
            }
            double slope = (vals[i] - vals[i-1]) / (times[i] - times[i-1]);
            if( (i==1 && timeStamp <= times[i])  ||
                (i==(vals.Length-2) && timeStamp>times[i])){
                iVal = vals[i-1] + (timeStamp - times[i-1]) * slope;
                return iVal;
            }
            double a0, a1, a2, a3, mu2;
            double mu;
            int j;
            if (timeStamp <= times[i])
                j = i;
            else
                j = i + 1;
            if (times[j] == times[j - 1])
            {
                mu = 1;
            }
            else
                mu = (timeStamp - times[j-1]) / (times[j] - times[j-1]);
            mu2 = mu * mu;
            //a0 = y3 - y2 - y0 + y1;
            a0 = vals[j + 1] - vals[j] - vals[j - 2] + vals[j-1];
            //a1 = y0 - y1 - a0;
            a1 = vals[j - 2] - vals[j-1] - a0;
            //a2 = y2 - y0;
            a2 = vals[j] - vals[j - 2];
            //a3 = y1;
            a3 = vals[j-1];

            iVal = (a0 * mu * mu2 + a1 * mu2 + a2 * mu + a3);

            return iVal;
        }

        public static double[] filterData(double[] data, int nFilterPoints)
        {
            //This needs to be replaced with a proper general nPoints moving average filter
            //that takes into account the special characteristics of the touch surface signal
            //and the possibility of a small number of points.
            if (nFilterPoints == 0)
                return data;
            else if (nFilterPoints == 3)
                return filterData3(data);
            else
                return filterData5(data);
        }

        //This needs to be replaced with a proper general nPoints moving average filter
        //that takes into account the special characteristics of the touch surface signal
        //and the possibility of a small number of points.
        private static double[] filterData3(double[] data)
        {
            if(data.Length<=1)
                return data;
            double[] fd = new double[data.Length];
            fd[0] = (data[0]+data[1])/2;
            int i;
            for (i = 1; i < (data.Length-1); i++)
            {
                fd[i] = (data[i - 1] + data[i] + data[i + 1]) / 3;
            }
            fd[i] = (data[i - 1] + data[i]) / 2;

            return fd;
        }

        //This needs to be replaced with a proper general nPoints moving average filter
        //that takes into account the special characteristics of the touch surface signal
        //and the possibility of a small number of points.
        private static double[] filterData5(double[] data)
        {
            if (data.Length <= 4)
                return data;
            double[] fd = new double[data.Length];
            fd[0] =  (data[0] + data[1] + data[2]) / 3;
            fd[1] =  (data[0] + data[1] + data[2] + data[3]) / 4;
            int i;
            for (i = 2; i < (data.Length - 2); i++)
            {
                fd[i] = (data[i - 2] + data[i - 1] + data[i] + data[i + 1] + data[i + 2]) / 5;
            }
            fd[i] = (data[i - 2] + data[i - 1] + data[i]+data[i+1]) / 4;
            fd[i + 1] = (data[i - 1] + data[i] + data[i + 1]) / 3;

            return fd;
        }

        public double magnitudeDifference(double[] d1, double[] d2, out double sigma)
        {
            double r12 = 0;
            double d1Av, d2Av, d1Sum, d2Sum;
            double sumNum,         //sum numerator sum((d1-d1Av).(d2-d2Av))
                    sumD1DeltaSqr,  //sum(d1-d1Av)2
                    sumD2DeltaSqr;  //sum(d2-d2Av)2

            d1Av = d2Av = 0;
            d1Sum = d2Sum = 0;
            sumNum = sumD1DeltaSqr = sumD2DeltaSqr = 0;

            for (int i = 0; i < d1.Length; i++)
            {
                d1Sum += d1[i];
                d2Sum += d2[i];

            }
            d1Av = d1Sum / d1.Length;
            d2Av = d2Sum / d2.Length;

            double val, dif1, dif2, dif1Sqr, dif2Sqr;
            for (int i = 0; i < d1.Length; i++)
            {
                dif1 = d1[i] - d1Av;
                dif2 = d2[i] - d2Av;

                val = dif1 * dif2;
                sumNum += val;

                dif1Sqr = Math.Pow(dif1, 2);
                sumD1DeltaSqr += dif1Sqr;

                dif2Sqr = Math.Pow(dif2, 2);
                sumD2DeltaSqr += dif2Sqr;
            }
            r12 = sumNum / Math.Sqrt(sumD1DeltaSqr * sumD2DeltaSqr);

            // Now double check that the energy in the signal makes sense
            int threshold = int.MaxValue;


            // calculate the difference and find the total difference
            double[] diff = new double[d1.Length];
            double totalDifference = 0;
            for (int i = 0; i < diff.Length; i++)
            {
                //diff[i] = Math.Abs(d1[i] - d2[i]);
                if (d1[i] >= d2[i])
                    diff[i] = Math.Abs(d1[i] - d2[i]);
                else if (d1[i] < d2[i])
                    diff[i] = Math.Abs(d2[i] - d1[i]);
                totalDifference += diff[i];
            }

            // find the mean
            double mean = totalDifference / diff.Length;

            // find the variance
            double[] squaredMeanDifference = new double[d1.Length];
            double total = 0;
            for (int i = 0; i < squaredMeanDifference.Length; i++)
            {
                squaredMeanDifference[i] = Math.Pow(diff[i] - mean, 2);
                total += squaredMeanDifference[i];
            }
            double variance = total / squaredMeanDifference.Length;
            
            // find the standard deviation
            sigma = Math.Sqrt(variance);
            

            return totalDifference;
        }

        public double getCorrelation(double[] d1, double[] d2)
        {
            double  r12 = 0;
            double  d1Av, d2Av, d1Sum, d2Sum;
            double  sumNum,         //sum numerator sum((d1-d1Av).(d2-d2Av))
                    sumD1DeltaSqr,  //sum(d1-d1Av)2
                    sumD2DeltaSqr;  //sum(d2-d2Av)2

            d1Av = d2Av = 0;
            d1Sum = d2Sum = 0;
            sumNum = sumD1DeltaSqr = sumD2DeltaSqr = 0;

            for (int i = 0; i < d1.Length; i++)
            {
                d1Sum += d1[i];
                d2Sum += d2[i];

           } 
            d1Av = d1Sum / d1.Length;
            d2Av = d2Sum / d2.Length;

            double val, dif1, dif2, dif1Sqr, dif2Sqr;
            for (int i = 0; i < d1.Length; i++)
            {
                dif1 = d1[i] - d1Av;
                dif2 = d2[i] - d2Av;

                val = dif1 * dif2;
                sumNum += val;

                dif1Sqr = Math.Pow(dif1, 2);
                sumD1DeltaSqr += dif1Sqr;

                dif2Sqr = Math.Pow(dif2, 2);
                sumD2DeltaSqr += dif2Sqr;
            }
            r12 = sumNum / Math.Sqrt(sumD1DeltaSqr * sumD2DeltaSqr);

            // Now double check that the energy in the signal makes sense
            int threshold = int.MaxValue;
            if ((sumD1DeltaSqr / sumD2DeltaSqr) > threshold || (sumD2DeltaSqr / sumD1DeltaSqr) > threshold)
            {
                //Console.WriteLine("sd1/sd2: " + (sumD1DeltaSqr / sumD2DeltaSqr) + ", sd2/sd1: " + (sumD2DeltaSqr / sumD1DeltaSqr));
                return -1;
            }

            return r12;
        }

        #region notNeededRotationSutff

        private double getRegressionB1(double[] xs, double[] ys, int n)
        {
            double[] xy = new double[n];
            double[] xSqr = new double[n];
            double sumX = 0,
                    sumY = 0;
            double sumXY = 0,
                    sumXSqr = 0;
            double Sxy, Sxx;
            for (int i = 0; i < n; i++)
            {
                sumX += xs[i];
                sumY += ys[i];
                sumXY += xs[i] * ys[i];
                xSqr[i] += xs[i] * xs[i];
                sumXSqr += xSqr[i];
            }
            Sxy = sumXY - (sumX * sumY) / n;
            Sxx = sumXSqr - (sumX * sumX) / n;
            double B1 = 0;
            if (Sxx != 0)
                B1 = Sxy / Sxx;
            return B1;
        }
        #endregion
    }
    
}
